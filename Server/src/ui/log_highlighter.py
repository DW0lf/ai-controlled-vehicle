from PyQt5.QtCore import pyqtSlot, QRegExp
from PyQt5.QtGui import QColor, QRegExpValidator, QSyntaxHighlighter, QTextCharFormat, QFont, QTextDocument

def format(color: str, style='') -> QTextCharFormat:     
    """Return a QTextCharFormat with the given attributes.
    Source: https://wiki.python.org/moin/PyQt/Python%20syntax%20highlighting
    
    Args:
        color(str): color as string (all SVG 1.0 color names are valid)
        style(str): string specifying the text style
    
    Returns:
        QTextCharFormat: object specifying the highlighting
    """
    _color = QColor()
    _color.setNamedColor(color)
 
    _format = QTextCharFormat()
    _format.setForeground(_color)
    if 'bold' in style:
        _format.setFontWeight(QFont.Bold)
    if 'italic' in style:
        _format.setFontItalic(True)
 
    return _format

# Syntax styles for format method
STYLES = {
    'headline': format('black', 'bold'),
    'numbers': format('red')
}

class LogHighlighter(QSyntaxHighlighter):
    def __init__(self, parent : QTextDocument):
        QSyntaxHighlighter.__init__(self, parent)
        self.parent = parent
        
        # rule for bracket expressions
        rules = [(r"\[[\w\]*\s*[\w]*\]:", 0, STYLES['headline'])]

        self.rules = [(QRegExp(pat), index, fmt) for (pat, index, fmt) in rules]
    
    def highlightBlock(self, text: str):
        """Apply syntax highlighting to the given block of text.
        """
        # Do other syntax formatting
        for expression, nth, form in self.rules:
            index = expression.indexIn(text, 0)

            while index >= 0:
                # We actually want the index of the nth match
                index = expression.pos(nth)
                expr = expression.cap(nth)
                length = len(expr)
                self.setFormat(index, length, form)
                index = expression.indexIn(text, index + length)
        self.setCurrentBlockState(0)
