#!/usr/bin/env python3

"""Implement the controling of ultrasonic sensor."""

import RPi.GPIO as GPIO
import time
from collections import deque

class UltrasonicSensor(object):
    def __init__(self, echo_pin, trigger_pin, name="Ultrasonic sensor"):
        """Initialize ultrasonic sensor.

        :parameter
        echo_pin    --  pin number of echo pin to evaluate (int)
        trigger_pin --  pin number of trigger pin to trigger for signal (int)
        name        --  name of sensor (string)"""
        self.echo = echo_pin
        self.trigger = trigger_pin
        self.name = name
        self.values = deque(maxlen=10)

        try:
            GPIO.setmode(GPIO.BOARD)

            GPIO.setup(self.trigger, GPIO.OUT)
            GPIO.setup(self.echo, GPIO.IN)
            GPIO.output(self.trigger, GPIO.LOW)
            time.sleep(2)
        except BaseException:
            GPIO.cleanup()

    def get_distance(self):
        """Get the distance for the sensor.

        :return
        distance -- distance for sensor as float"""
        try:
            GPIO.output(self.trigger, GPIO.HIGH)
            time.sleep(0.00001)
            GPIO.output(self.trigger, GPIO.LOW)
            pulse_end_time = 0

            while GPIO.input(self.echo) == 0:
                pulse_start_time = time.time()
            while GPIO.input(self.echo) == 1:
                pulse_end_time = time.time()

            if not pulse_end_time:
                return
            pulse_duration = pulse_end_time - pulse_start_time
            distance = round(pulse_duration * 17150, 2)
            self.values.append(distance)
        except BaseException as e:
            print(e)

    def __del__(self):
        GPIO.cleanup()


if __name__ == '__main__':
    sensor = UltrasonicSensor(35, 40)
    count = 0
    while True:
        try:
            count += 1
            sensor.get_distance()
            if count and not count % 10:
                values = [i for i in sensor.values]
                print("Distance: %f" % (sum(values)/len(values)))
            time.sleep(0.1)
        except BaseException:
            del sensor
            break
    exit()
