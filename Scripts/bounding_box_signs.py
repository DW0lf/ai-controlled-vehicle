from functools import partial
import numpy as np
import cv2
import time
import numba
from matplotlib import pyplot as plt


# -------- PARAMETERS -------- 
# image parameters
# TODO: Read from config
IMG_WIDTH = 640
IMG_HEIGHT = 360

# TODO: read number of max detected objects from config
MAX_DETECTED_OBJECTS = 4    # maximal number of objects to detect
ITERATIONS_PER_OBJECT = 2   # number of vertical lines to try for an object to detect 
                            # (this count is finally multiplied with MAX_DETECTED_OBJECTS)

# RANSAC
MINIMAL_Y_DIFF = 40 # minimum length of line segment to return by ransac
MAX_X_DIFF = 10 # maximum distance between x values of line points returned by ransac

# maximize line and updating point cloud
Y_GAP_THRESH = 5    # maximal gap between two pixels to be joined to one line (number of black lines)
X_WINDOW = 6        # window size in x-direction, this window is moved upwards 
                    # and downwards to detect other pixels belonging to the line 
                    # (total size in x-direction 2*X_WINDOW+1) 
                    # 
                    # It also defines the padding area around a line in which the point cloud
                    # get's cleaned up 
                    
# determine bounding boxes to return
BOUNDING_BOX_Y_OFFSET = 0.15 # % of line length which will be deducted from line end
BOUNDING_BOX_SIZE = 0.7 # % of line length 


# -------- PLOT / SAVE SETTINGS --------
display_images = True  # display images with determined bounding boxes
display_thresh = False # display threshold images
save_images = True  # save images with determined bounding boxes
display_lines = False    # display detected vertical lines

def timing_wrapper(func_call, repetitions):
    times = []
    for i in range(repetitions):
        t0 = time.time()
        res = func_call()
        times.append(time.time()-t0)
    return res, np.average(times), np.std(times)


@numba.njit(cache=True)
def make_point_cloud(binary_img : np.ndarray) -> np.ndarray:
    """Generates point cloud from binary image. All elements is point 
    cloud constist of x,y coordinates of active pixels.
    
    Args:
        binary_img(np.ndarray): binary image to detect white pixels in
    
    Returns:
        np.ndarray: nx2 numpy array containing y and x coordinates of active pixels
    """
    y_val, x_val = np.nonzero(binary_img)
    cloud = np.column_stack((y_val,x_val))
    cloud = cloud[cloud[:, 1].argsort()] # sort cloud after x
    return cloud

@numba.njit(cache=True)
def points_valid(point0 : np.ndarray, point1 : np.ndarray) -> bool:
    """Calculates if the two points are valid to find a vertical line.

    Args:
        point0 (np.ndarray): first point
        point1 (np.ndarray): second point

    Returns:
        bool: are two points nearly vertical and is the distance between them
              above \"MINIMAL_Y_DIFF\"
    """
    # compute euclidian distance
    dist = np.linalg.norm((point0-point1).astype(np.float32))
    # compute difference of x values
    x_diff = np.abs(point0[1]-point1[1])

    return (x_diff < MAX_X_DIFF) and (dist > MINIMAL_Y_DIFF)

@numba.njit(cache=True)
def random_vertical_line(point_cloud : np.ndarray) -> np.ndarray:
    """Find two random, nearly vertical points with RANSAC algorithm.

    Args:
        point_cloud (np.ndarray): array with pixel coordinates (y_val, x_val) to choose from

    Returns:
        np.ndarray: two points with the smaller y as the second point, e.g. (60,120,80,110) (y_start, x_start, y_end, x_end)
    """    
    max_iterations = point_cloud.shape[0] - 1
    while max_iterations:
        max_iterations -= 1
        # receive two random indices from point cloud
        indices = np.random.choice(point_cloud.shape[0], 2, replace=False)
        
        p0 = point_cloud[indices[0]]
        p1 = point_cloud[indices[1]]
        if points_valid(p0,p1):
            break
    
    # place smaller y at the end, important for \"maximize_line\"
    if p1[0] - p0[0] < 0:
        p0, p1 = p1, p0
    if max_iterations > 0:
        return np.array([p1[0], p1[1], p0[0], p0[1]], dtype=np.int16)
    else:
        # int because slope calc in maximize line... see main method
        return np.zeros((4,), dtype=np.int16) 

@numba.njit(cache=True)
def maximize_line(line : np.ndarray, binary_img : np.ndarray) -> np.ndarray:
    """Maximizes line segment by moving end point upwards and searching for 
    start point by moving sliding window downwards .

    Args:
        line (np.ndarray): one line defined by 4 points with structure (y_start, x_start, y_end, x_end)
        binary_img (np.ndarray): binary image as numpy array

    Returns:
        np.ndarray: maximized line segments or numpy array containing zeros
    """
    # smaller y is end point (at the top)    
    line_start = line[:2] # bottom point
    line_end = line[2:] # top point

    # how much pixel in x direction per y
    # negative if start point is left of end point (smaller x)
    inv_slope = (line_start[1] - line_end[1]) / (line_start[0] - line_end[0])
    
    # move end point (top) upwards to check for gaps
    gap_counter = Y_GAP_THRESH
    old_line_end = line_end # save line end for later
    y,x_mid = line_end
    x_new = x_mid
    
    while gap_counter > 0:
        y -= 1
        x_mid = int(np.rint(x_mid - inv_slope))
        
        # the following condition and loop build the sliding window with a width 
        # of X_WINDOW*2+1 pixels and move it upwards
        # e.g. if X_WINDOW is equal to 2 than at the middle element (index 2) of 
        # the binary image is checked at first, afterwards at index 1 and 3 and 
        # finally at index 0 and 5 
        if binary_img[y, x_mid]:
            # check middle (most likely positon) of sliding window
            x_new = x_mid
            gap_counter = Y_GAP_THRESH
        else:
            # check other positions of sliding window
            for x_win in range(1, X_WINDOW):
                if binary_img[y, x_mid + x_win]:
                    # active pixel found
                    x_new = x_mid + x_win
                    gap_counter = Y_GAP_THRESH
                    break
                if binary_img[y, x_mid - x_win]:
                    # active pixel found 
                    x_new = x_mid - x_win
                    gap_counter = Y_GAP_THRESH
                    break
                
        gap_counter -= 1
    
    # update line end with correct coordinates 
    line_end = y + Y_GAP_THRESH, x_new

    # traverse downwards to find and maximize start point
    meet_line_start = False
    gap_counter = Y_GAP_THRESH
    y, x_mid = old_line_end
    
    while gap_counter > 0:
        y += 1
        x_mid = int(np.rint(x_mid + inv_slope))
        
        # refer to above explanation
        if binary_img[y, x_mid]:
            x_new = x_mid
            gap_counter = Y_GAP_THRESH
        else:
            for x_win in range(1, X_WINDOW):
                if binary_img[y, x_mid + x_win]:
                    # active pixel found
                    x_new = x_mid + x_win
                    gap_counter = Y_GAP_THRESH
                    break
                if binary_img[y, x_mid - x_win]:
                    # active pixel found
                    x_new = x_mid - x_win
                    gap_counter = Y_GAP_THRESH
                    break

        gap_counter -= 1

    # if y is great enough line start was met
    if (y - Y_GAP_THRESH) > line_start[0]:
        # return line if it includes initial start and end point
        line_start = y - Y_GAP_THRESH, x_new
        return np.array([line_start[0], line_start[1], line_end[0], line_end[1]], dtype=np.int16)
    else:
        # return arrays of zeros if initial start and end points are not included
        return np.zeros((4,), dtype=np.int16) 

@numba.njit(cache=True)
def calculate_bounding_boxes(lines : np.ndarray) -> np.ndarray:
    """Computes bounding boxes at line endings.
    
    Args:
        lines(np.ndarray): Filtered lines with following structure (start_y, start_x, end_y, end_x)
    Returns:
        np.ndarray: shape[len(lines),4] with left lower corner (y,x) and right upper corner (y,x)
    """
    bounding_boxes = np.zeros((lines.shape[0],4), dtype=np.int16)

    # a sign is 10cm tall and the sign itself is 3cm
    for idx,line in enumerate(lines):
        if not np.any(line):
            continue
        length = line[2] - line[0]
        # use line end only!
        p0_y = line[2] - int(length*BOUNDING_BOX_Y_OFFSET)
        p0_x = line[3] - int(length*BOUNDING_BOX_SIZE) // 2
        p1_y = p0_y + int(length*BOUNDING_BOX_SIZE)
        p1_x = p0_x + int(length*BOUNDING_BOX_SIZE)
        # skip bounding boxes which exceed image borders
        if (p1_y or p0_x) < 0 or p1_x > IMG_WIDTH:
            continue
        bounding_boxes[idx] = p0_y, p0_x, p1_y, p1_x
    

    return bounding_boxes

@numba.njit(cache=True)
def update_point_cloud(cloud : np.ndarray, line : np.ndarray) -> np.ndarray:
    """Updates point cloud by removing all points which coordinates are inside 
    a rectangular window, specified by a found line.
    
    Args:
        cloud (np.ndarray): point_cloud sorted after y
        line (np.ndarray): array which describes a line (y_start,x_start, y_end,x_end)
    
    Returns:
        np.ndarray: new point cloud
    """
    # if line is empty (0 only) then there is nothing to do
    if not np.any(line):
        return cloud
    
    line_start = line[:2].copy()
    line_end = line[2:].copy()
    
    # create padded rectangle around line 
    if line_start[1] > line_end[1]:
        line_start[1] = line_start[1] + X_WINDOW
        line_end[1] = line_end[1] - X_WINDOW
        idx_start = np.searchsorted(cloud[:,1], line_end[1], side='left')
        idx_end = np.searchsorted(cloud[:,1], line_start[1], side='right')
    elif line_start[1] < line_end[1]:
        line_start[1] = line_start[1] - X_WINDOW
        line_end[1] = line_end[1] + X_WINDOW
        idx_start = np.searchsorted(cloud[:,1], line_start[1], side='left')
        idx_end = np.searchsorted(cloud[:,1], line_end[1], side='right')
    # x values the same --> bigger Padding Area
    else:
        line_start[1] = line_start[1] - X_WINDOW*2
        line_end[1] = line_end[1] + X_WINDOW*2
        idx_start = np.searchsorted(cloud[:,1], line_start[1], side='left')
        idx_end = np.searchsorted(cloud[:,1], line_end[1], side='right')

    mask = np.ones((cloud.shape[0],), dtype=np.uint16)

    # delete all points in cloud columnwise
    for idx in range(idx_start, idx_end):
        mask[idx] = 0

    return cloud[mask > 0]

def find_sign_with_v_line(img : np.ndarray) -> np.ndarray:
    """[summary]

    Args:
        img (np.ndarray): image in which vertical lines are present

    Returns:
        np.ndarray: sqaure bounding boxes in which traffic signs are suspected 
    """
    bounding_boxes = np.zeros((MAX_DETECTED_OBJECTS, 4))

    # process image, 
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    
    # apply sobel to detect vertical lines and binarize image
    v_lines = cv2.Sobel(gray,cv2.CV_8UC1,1,0,ksize=3, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT)
    _, thresh = cv2.threshold(v_lines, 100, 255, cv2.THRESH_BINARY)
    thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN,(3,3))
    #thresh = cv2.dilate(thresh, (3,3), iterations=1)
    
    if display_thresh:
        plt.figure(1)
        plt.imshow(thresh)
        plt.show()
        #cv2.imshow("Thresh",thresh)
        #cv2.waitKey(0)
    
    # create initial point cloud as coordinates of white pixels in binary threshold image
    cloud = make_point_cloud(thresh)

    # lines not of type uint, because maximize_line can produce negative x in slope calc
    lines = np.zeros([MAX_DETECTED_OBJECTS * ITERATIONS_PER_OBJECT, 4], dtype=np.int16) 
    for i in range(MAX_DETECTED_OBJECTS * ITERATIONS_PER_OBJECT):
        lines[i] = random_vertical_line(cloud)
        if not np.any(lines[i]):
            # RANSAC does not find any line which satisfies the requirements and was not considered already
            continue

        lines[i] = maximize_line(lines[i], thresh)

        cloud = update_point_cloud(cloud, lines[i])

    bounding_boxes = calculate_bounding_boxes(lines)
    if display_lines and display_images:
        for idx, line in enumerate(lines):
            if np.any(line):
                print("----------%d-----------" % idx)
                print(line)
                print("\n")
                cv2.line(img, (line[1],line[0]),(line[3],line[2]), (0,0,255),4)

        for box in bounding_boxes:
            cv2.rectangle(img, (box[1],box[0]), (box[3],box[2]), (0,255,0),thickness=2)

        if display_lines:
            cv2.imshow("Vertical lines", img)
            cv2.waitKey(0)
    return bounding_boxes

def scale_bounding_box(img : np.ndarray, bounding_box : np.ndarray, target_size : int) -> np.ndarray:
    """Scale square bounding box to \"target_size\"

    Args:
        img (np.ndarray): color image from camera
        bounding_box (np.ndarray): single detected bounding box
        target_size (int): target size in px (e.g. 32 leads to 32x32 image )

    Returns:
        np.ndarray: [description]
    """
    y0, x0, y1, x1 = bounding_box
    height = y0 - y1
    width = x0 - x1
    channels = img.shape[2]
    assert(height == width) # has to be sqaure bounding box
    
    scaling_factor = target_size / width
    if scaling_factor > 1.0:
        interpolation_mode = cv2.INTER_LINEAR # zooming
    else:
        interpolation_mode = cv2.INTER_AREA # shrinking
    
    cropped_img = np.zeros((height,width,channels))
    cropped_img = img[y1:y0,x1:x0]

    return cv2.resize(cropped_img,None,fx=scaling_factor,
                        fy=scaling_factor,interpolation=interpolation_mode)

if __name__ == "__main__":
    # stop sign daylight warm
    scenarios = [('left_right_daylight_cold.png', "Left and right daylight cold"),
                 #('left_right_daylight_warm.png', "Left and right daylight warm"),
                 #("right_daylight_warm.png", "Right daylight warm"),
                 #("stop_daylight_cold.png", "Stop sign daylight cold"),
                 ('stop_daylight_warm.png', "Stop sign daylight warm")
                 ]
    scenarios_traffic_light =  [('Traffic_signs/traffic_light_green_cold.png', "Traffic Light Green Cold"),
                                ('Traffic_signs/traffic_light_green.png', "Traffic Light Green"),
                                ('Traffic_signs/traffic_light_red_cold.png', "Traffic Light Red Cold"),
                                ('Traffic_signs/traffic_light_red.png', "Traffic Light Red"),
                                ('Traffic_signs/traffic_light_yellow_cold.png', "Traffic Light Yellow Cold"),
                                ('Traffic_signs/traffic_light_yellow.png', "Traffic Light Yellow Cold")]
    for scene in scenarios:
        img_name, scene_name = scene
        img = cv2.imread(img_name)
        print(img.shape)
        print(img[0])
        print(scene_name)
        t1 = time.time()
        bounding_box = find_sign_with_v_line(img)
        #print("Time to compile:", time.time()-t1)
        #p1 = partial(find_sign_with_v_line, img)
        #bounding_box, avg, std = timing_wrapper(p1,100)
        #print("Avg: %fms" % (avg*1000))
        #print("Std: %fms" % (std*1000))
        print("\n")
        for box in bounding_box:
            cv2.rectangle(img, (box[1],box[0]), (box[3],box[2]), (0,255,0),thickness=2)
        
        if display_images:
            print(bounding_box)
            cv2.imshow("Bounding Boxes " + scene_name, img)
            cv2.waitKey(0)
            for box in bounding_box:
                if not any(box):
                    continue
                cropped = scale_bounding_box(img,box,32)
                cv2.imshow("Cropped",cropped)
                cv2.waitKey(0)
        if save_images:
            cv2.imwrite(img_name[:-4] +"_bounding_box.png", img)

