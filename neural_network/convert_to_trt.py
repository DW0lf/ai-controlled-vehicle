from functools import partial
import numpy as np
import time
import torch
import model as custom_model
from torch2trt import torch2trt, TRTModule

BATCH_SIZE = 2
CHECK_DIFF = False
CONVERT = False
MEASURE_PERFORMANCE = True
ITER = 30
REPETITIONS = 100

def timing_wrapper(func_call, repetitions):
    times = []
    for _ in range(repetitions):
        t0 = time.time()
        res = func_call()
        times.append(time.time()-t0)
    return res, np.average(times), np.std(times)

def inference(model, data, iter):
    for _ in range(iter):
        model(data)


# load original network
t1 = time.time()
if CONVERT:
    model = custom_model.WideResNet(6, 10, 4, 0)
    model.load_state_dict(torch.load('wrn_10_4.pth'))
    model = model.eval().cuda()

    # convert to TensorRT
    x = torch.ones((BATCH_SIZE, 3, 32, 32), dtype=torch.float32).cuda()
    model_trt = torch2trt(model, [x], max_batch_size=BATCH_SIZE, fp16_mode=True)
else:
    model_trt = TRTModule()
    model_trt.load_state_dict(torch.load('wrn_10_4_trt.pth'))
    model_trt = model_trt.eval().cuda()
    x = torch.ones((BATCH_SIZE, 3, 32, 32), dtype=torch.float32).cuda()
t2 = time.time()

print("MODEL LOADED! (%.3f s)" % (t2-t1))
print("CUDA ENGINE BATCHSIZE:", model_trt.engine.max_batch_size)

# check max diff against original network
if CONVERT and CHECK_DIFF:
    y = model(x)
    y_trt = model_trt(x)
    print("MAX DIFF:", torch.max(torch.abs(y - y_trt)))

# benchmark of converted neural network
if MEASURE_PERFORMANCE:
    _, inference_avg, inference_std = timing_wrapper(partial(inference, model_trt, x, ITER), REPETITIONS)
    print("BENCHMARK: %.3f ms (+- %3f ms)" % (inference_avg * 1000 / ITER, inference_std * 1000 / ITER))

# store converted model
torch.save(model_trt.state_dict(), 'wrn_trt.pth')