from collections import OrderedDict

import torch
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter, MaxNLocator

path = "./"
tex_textwidth = 448.13095

accepted_layer_types = {torch.nn.modules.conv.Conv2d: 0,
                        torch.nn.modules.linear.Linear: 1}
layer_type_mapping = {0: ("Conv", "red"), 1: ("FC", "blue")}

plt_params = {
    "text.usetex": True,
    "font.family": "sans-serif",
    "axes.labelsize": 10,
    "font.size": 10,
    "legend.fontsize": 8,
    "xtick.labelsize": 8,
    "ytick.labelsize": 8
}

plt.rcParams.update(plt_params)


def get_parameter_distribution(model):
    layer_types = list()
    for n, sm in model.named_modules():
        layer_type = type(sm)
        if layer_type not in accepted_layer_types or "downsample" in n:
            continue
        layer_types.append(accepted_layer_types[layer_type])

    layer_param_count = OrderedDict()
    for n, p in model.named_parameters():
        ln = n.rsplit(sep='.', maxsplit=1)[0]
        if "bn" in ln or "downsample" in ln:
            lastLayer = next(reversed(layer_param_count.keys()))
            layer_param_count[lastLayer] += p.numel()
            continue
        if ln not in layer_param_count:
            layer_param_count[ln] = p.numel()
        else:
            layer_param_count[ln] += p.numel()

    total_param_cnt = sum(list(layer_param_count.values()))
    if len(layer_types) != len(layer_param_count):
        raise ValueError(len(layer_types), "is unequal to", len(layer_param_count))

    layer_param_percentage = OrderedDict()
    layer_param_abs = OrderedDict()
    for n, p_cnt in layer_param_count.items():
        layer_param_percentage[n] = p_cnt / total_param_cnt
        layer_param_abs[n] = p_cnt

    return layer_param_percentage, layer_param_abs, layer_types


def set_size(width, fraction=1):
    """Set figure dimensions to avoid scaling in LaTeX (cf. https://jwalton.info//Embed-Publication-Matplotlib-Latex/)

    Parameters
    ----------
    width: float
            Document textwidth or columnwidth in pts
    fraction: float, optional
            Fraction of the width which you wish the figure to occupy

    Returns
    -------
    fig_dim: tuple
            Dimensions of figure in inches
    """
    # Width of figure (in pts)
    fig_width_pt = width * fraction

    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * golden_ratio

    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim


def create_model_figures(model):
    rel_data, abs_data, layer_types = get_parameter_distribution(model)
    layer_types_set = set(layer_types)

    # create rel distribution figure
    for layer_type in layer_types_set:
        x_data = [idx for idx, val in enumerate(layer_types) if val == layer_type]
        y_data = [val for idx, val in enumerate(rel_data.values()) if layer_types[idx] == layer_type]
        plt.bar(x_data, y_data, label=layer_type_mapping[layer_type][0], color=layer_type_mapping[layer_type][1])

    plt.grid(True, which="both", zorder=0)
    plt.xlabel("Layer index")
    plt.ylabel("Parameter percentage")

    axis = plt.gca()
    axis.legend(borderpad=1, handlelength=3, handletextpad=1, columnspacing=2, edgecolor="black",
                fancybox=False, loc='center', bbox_to_anchor=(0.5, -0.5),
                ncol=len(layer_types_set)).get_frame().set_linewidth(1)
    axis.set_ylim([0.0, 1.2*max(list(rel_data.values()))])
    axis.set_axisbelow(True)
    axis.tick_params(axis='both', direction='out', which='major')
    axis.xaxis.set_major_locator(MaxNLocator(integer=True))
    axis.yaxis.set_major_formatter(PercentFormatter(1))

    fig = plt.gcf()
    fig_size = set_size(tex_textwidth, 0.5)
    fig.set_size_inches(*fig_size)

    filename = path + model._get_name().lower().replace("-", "") + "_rel_param_dist.pdf"
    fig.savefig(filename, bbox_inches='tight')
    plt.clf()

    # create abs distribution figure
    for layer_type in layer_types_set:
        x_data = [idx for idx, val in enumerate(layer_types) if val == layer_type]
        y_data = [val for idx, val in enumerate(abs_data.values()) if layer_types[idx] == layer_type]
        plt.bar(x_data, y_data, label=layer_type_mapping[layer_type][0], color=layer_type_mapping[layer_type][1])

    plt.grid(True, which="both", zorder=0)
    plt.xlabel("Layer index")
    plt.ylabel("Parameter count")
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

    axis = plt.gca()
    axis.legend(borderpad=1, handlelength=3, handletextpad=1, columnspacing=2, edgecolor="black",
                fancybox=False, loc='center', bbox_to_anchor=(0.5, -0.5),
                ncol=len(layer_types_set)).get_frame().set_linewidth(1)
    #axis.set_ylim([0.0, 1.2*max(list(abs_data.values()))])
    axis.set_axisbelow(True)
    axis.tick_params(axis='both', direction='out', which='major')
    axis.xaxis.set_major_locator(MaxNLocator(integer=True))
    axis.set_yscale('log')

    fig = plt.gcf()
    fig_size = set_size(tex_textwidth, 0.5)
    fig.set_size_inches(*fig_size)

    filename = path + model._get_name().lower().replace("-", "") + "_abs_param_dist.pdf"
    fig.savefig(filename, bbox_inches='tight')
    plt.clf()


def create_all_figures(models):
    for model in models:
        create_model_figures(model)