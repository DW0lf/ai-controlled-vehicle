import copy
from datetime import datetime 
from typing import Any, List, Tuple

import torch
from torch.tensor import Tensor
from torch.nn import Module
from torch.optim import Optimizer
from torch.utils.data import DataLoader

import matplotlib.pyplot as plt


def train_adam(train_loader: DataLoader, model: Module, loss_func: Module, optimizer: Optimizer, lr_scheduler: Any, device: str) -> Tuple[Module, Optimizer, float, float]:
    """
    Performs training per epoch.

    Args:
        train_loader (DataLoader): DataLoader that provides training data
        model (Module): Neural network
        loss_func (Callable): Loss function
        optimizer (Optimizer): Optimization algorithm
        lr_scheduler (Any): Learning rate scheduler
        warmup_scheduler (Any): Warm-up scheduler
        device (str): Device where training is performed on

    Returns:
        Tuple[Module, Optimizer, float, float]: Trained neural network, (adapted) optimization algorithm, loss after 
        epoch, training accuracy after one epoch
    """

    model.train()
    running_loss = 0
    running_acc = 0
    
    for X, y_true in train_loader:

        optimizer.zero_grad()
        
        X = X.to(device)
        y_true = y_true.to(device)
    
        # forward pass
        y_pred = model(X)
        
        # loss computation
        loss = loss_func(y_pred, y_true) 
        running_loss += loss.item() * X.size(0)
        
        # accuracy computation
        acc = get_accuracy(y_true, y_pred)
        running_acc += acc.item() * X.size(0)

        # backward pass
        loss.backward()
        optimizer.step()
        
        lr_scheduler.step()
        
    epoch_loss = running_loss / len(train_loader.dataset)
    epoch_acc = running_acc / len(train_loader.dataset)
    
    return model, optimizer, epoch_loss, epoch_acc


def validate(valid_loader: DataLoader, model: Module, loss_func: Module, device: str) -> Tuple[Module, float, float]:
    """
    Performs validation per epoch.

    Args:
        valid_loader (DataLoader): DataLoader that provides validation data
        model (Module): Neural network
        loss_func (Callable): Loss function
        device (str): Device where training is performed on

    Returns:
        Tuple[Module, float, float]: Trained neural network, loss after epoch, validation accuracy after one epoch
    """
   
    model.eval()
    running_loss = 0
    running_acc = 0
    
    for X, y_true in valid_loader:
    
        X = X.to(device)
        y_true = y_true.to(device)

        # forward pass
        y_pred = model(X) 
        
        # loss computation
        loss = loss_func(y_pred, y_true)
        running_loss += loss.item() * X.size(0)
        
        # accuracy computation
        acc = get_accuracy(y_true, y_pred)
        running_acc += acc.item() * X.size(0)

    epoch_loss = running_loss / len(valid_loader.dataset)
    epoch_acc = running_acc / len(valid_loader.dataset)
        
    return model, epoch_loss, epoch_acc


def get_accuracy(y_true: Tensor, y_pred: Tensor) -> Tensor:
    """
    Computes accuracy for given predictions and ground-truth values.

    Args:
        y_true (Tensor): Ground-truth values
        y_pred (Tensor): Predicted values

    Returns:
        Number: Accuracy
    """
    
    y_pred_softmax = torch.log_softmax(y_pred, dim = 1)
    _, y_pred_tags = torch.max(y_pred_softmax, dim = 1)    
    
    correct_pred = (y_pred_tags == y_true).float()
    acc = correct_pred.sum() / len(correct_pred)
    
    return acc


def plot_history(train_data: List[float], valid_data: List[float], type: str) -> None:
    """
    Plots history of training and validation results for accuracy/loss.

    Args:
        train_data (List[float]): Training results history
        valid_data (List[float]): Validation results history
        type (str): Results type (\"acc\" or \"loss\")
    """
    
    plt.plot(range(len(train_data)), train_data, color='r', label="Training")
    plt.plot(range(len(valid_data)), valid_data, color='b', label="Validation")
    plt.xlabel("Epoch")
    
    if type == "acc":
        plt.ylabel("Accuracy")
    elif type == "loss":
        plt.ylabel("Loss")

    plt.legend(loc='upper right')
        
    plt.show()
        

def training_loop(model: Module, loss_func: Module, optimizer: Optimizer, lr_scheduler: Any, train_loader: DataLoader, 
                  valid_loader: DataLoader, epochs: int, start_epoch: int, early_stopping_epochs: int, device: str, 
                  print_every: int = 1) -> Tuple[Module, Tuple[Optimizer, Optimizer], 
                                                 Tuple[List[float], List[float]], 
                                                 Tuple[List[float], List[float]]]:
    """
    Implements complete training process.

    Args:
        model (Module): Neural network
        loss_func (Module): Loss function
        optimizer (Optimizer): Optimization algorithm
        lr_scheduler (Any): Learning rate scheduler
        train_loader (DataLoader): DataLoader for training data
        valid_loader (DataLoader): DataLoader for validation data
        epochs (int): Number of training epochs
        start_epoch (int): Initial epoch at which training is started
        early_stopping_epochs (int): Early stopping of training, if number of epochs without improvement is exceeded
        device (str): Device where training is performed on
        print_every (int, optional): How often results are printed. Defaults to 1.

    Returns:
        Tuple[Module, Optimizer, Tuple[float, float], Tuple[float, float]]: Trained neural network, 
        adapted optimizer, training/validation loss history, training/validation accuracy history
    """
    
    # metrics history
    train_loss_history = []
    train_acc_history = []
    valid_loss_history = []
    valid_acc_history = []

    best_model = None
    min_loss = 1e8
    max_acc = 0.
    epochs_without_improvement = 0
 
    # train model
    for epoch in range(start_epoch, epochs):
        model, adam_optimizer, train_loss, train_acc = train_adam(train_loader, model, loss_func, optimizer, 
                                                                  lr_scheduler, device)
        
        train_loss_history.append(train_loss)
        train_acc_history.append(train_acc)

        # validation
        with torch.no_grad():
            model, valid_loss, valid_acc = validate(valid_loader, model, loss_func, device)
            
            # early stopping
            if early_stopping_epochs >= 0:
                if (len(valid_loss_history) > 0 and valid_loss >= min_loss) or (len(valid_acc_history) > 0 and valid_acc <= max_acc):
                    epochs_without_improvement += 1
                else:
                    epochs_without_improvement = 0
                    best_model = copy.deepcopy(model.state_dict())
                
                if epochs_without_improvement > early_stopping_epochs:
                    print("EARLY STOPPING!")
                    break
            
            if valid_loss < min_loss:
                min_loss = valid_loss
            if valid_acc > max_acc:
                max_acc = valid_acc
            
            valid_loss_history.append(valid_loss)
            valid_acc_history.append(valid_acc)

        if epoch % print_every == (print_every - 1):
                
            print(f'{datetime.now().time().replace(microsecond=0)} --- '
                  f'Epoch: {epoch}\t'
                  f'Train loss: {train_loss:.4f}\t'
                  f'Valid loss: {valid_loss:.4f}\t'
                  f'Train accuracy: {100 * train_acc:.2f}\t'
                  f'Valid accuracy: {100 * valid_acc:.2f}')

    plot_history(train_acc_history, valid_acc_history, "acc")
    plot_history(train_loss_history, valid_loss_history, "loss")
    
    return best_model, optimizer, (train_loss_history, valid_loss_history), (train_acc_history, valid_acc_history)