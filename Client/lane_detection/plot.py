from typing import Tuple
import numpy as np
import numba
import cv2
import time
from functools import partial

import join_lines as jl
from calc_xyz import _calc_xy0_camera_center, CameraTransformer

img_roi = np.loadtxt("roi.csv", np.int32)

color_maps = [(0,0,255), (255,0,0), (0,255,0), (0,255,255), (255,0,255), (255,255,0), (0,128,255), (255,255,255)]

MAX_DETECABLE_OBJECTS = 16
roi_slice_x = slice(1, -1)
roi_slice_y = slice(img_roi[3]//3, None)
ROI_OFFSET_VEC = np.array([roi_slice_x.start, roi_slice_y.start, roi_slice_x.start, roi_slice_y.start], dtype=np.uint16)
ROI_TOP_LEFT = img_roi[0:2].astype(np.float32)
IMAGE_WIDTH = 640
IMAGE_HEIGHT = 360
DISTORTED_IMAGE_CENTER = np.array([IMAGE_WIDTH//2, IMAGE_HEIGHT//2], dtype=np.uint16)

# distance precision in pixel, i.e. 1 pixel
hough_rho = 3
# angular precision in radian, i.e. 1 degree
hough_min_angle = np.pi / 180
# number of votes needed to be considered a line segment
# if a line has more votes, Hough Transform considers them to be more likely to have detected a line segment
hough_min_votes = 50
# minimum length of the line segment in pixels
# Hough Transform won’t return any line segments shorter than this minimum length
hough_min_line_length = 20
# maximum in pixels that two line segments that can be separated and still be considered a single line segment
# for example, if we had dashed lane markers, by specifying a reasonable max line gap, Hough Transform will 
# consider the entire dashed lane line as one straight line, which is desirable
hough_max_line_gap = 5

MIN_VERTICAL_SLOPE = 0.2
MIN_VERTICAL_TRAJECTORY_SLOPE = 1 / MIN_VERTICAL_SLOPE
MIN_HORIZONTAL_LENGTH_PX = 100
PSEUDO_METERS_TO_PX = 150
MAX_TRAJECTORY_LENGTH_PX = 150
TRAJECTORY_Y_OFFSET_PX = 20
LANE_Y_MASK = np.array([0, 1, 0, 1], dtype=np.uint8)

# minimal distance of horizontal lane from car to stop
DISTANCE_TO_STOP = 0.25
# maximal distance of lines from car to take them into consideration for vertical lane based trajectory
DISTANCE_TO_STEER = 0.35
DISTANCE_WEIGTH = 0.3
LENGTH_WEIGTH = 0.7
assert DISTANCE_WEIGTH + LENGTH_WEIGTH == 1

def timing_wrapper(func_call, repetitions):
    times = []
    for _ in range(repetitions):
        t0 = time.time()
        res = func_call()
        times.append(time.time()-t0)
    return res, np.average(times), np.std(times)

def detect_line_segments(image: np.ndarray) -> np.ndarray:
    """
    Detects line segments in the image using Hough Transform algorithm.

    Args:
        image (np.ndarray): OpenCV input image

    Returns:
        List[np.ndarray[4]]: List of detected line segments (as [x0, y0, x1, y1] arrays)
    """
    hough_lines = cv2.HoughLinesP(image, hough_rho, hough_min_angle, hough_min_votes, 
                                    minLineLength=hough_min_line_length, 
                                    maxLineGap=hough_max_line_gap)
    if hough_lines is None:
        return np.zeros((1,4), dtype=np.uint16)
    else:
        return np.squeeze(hough_lines, 1).astype(np.uint16)

def filter_roi(image: np.ndarray) -> np.ndarray:
    """
    Filters region of interest (roi) in the image by cropping

    Args:
        image (np.ndarray): OpenCV input image

    Returns:
        np.ndarray: OpenCV image with filtered roi
    """
    
    return image[roi_slice_y, roi_slice_x]

def filter_edges(image: np.ndarray):
    """
    Filter edges in the image using Canny algorithm.

    Args:
        image (np.ndarray): OpenCV input image

    Returns:
        np.ndarray: OpenCV image with filtered edges
    """
    
    diff_rg = (image[:, :, 1]+54) - image[:, :, 2]
    avg = np.average(diff_rg) * 1.25

    gray = (diff_rg > avg).astype(np.uint8) * 255
    gray = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5)), iterations=1)
    
    return cv2.Canny(gray, 50, 150)

@numba.njit
def correct_lanes_offset(lanes: np.ndarray) -> np.ndarray:
    """
    Corrects offset of lanes that is caused by roi cropping.

    Args:
        lanes (np.ndarray): Uncorrected lanes

    Returns:
        np.ndarray: Corrected lanes
    """
    
    return lanes + ROI_OFFSET_VEC

@numba.njit
def get_lane_vec(lane: np.ndarray) -> np.ndarray:
    """
    Returns 2d vector in the camera frame that describes the lane. The returned vector is oriented from "bottom to top".

    Args:
        lane (np.ndarray): Lane input

    Returns:
        np.ndarray: Lane vector
    """
    return lane[0:2] - lane[2:4]

@numba.njit
def clip_lane_based_trajectory(trajectory: np.ndarray) -> None:
    """
    Limits length of the trajectory path computed from lanes. Clipping is performed in-place.

    Args:
        trajectory (np.ndarray): Unclipped trajectory path
    """
    
    trajectory_vec = get_lane_vec(trajectory)
    trajectory_length = np.linalg.norm(trajectory_vec)
    
    if trajectory_length > MAX_TRAJECTORY_LENGTH_PX:
        trajectory_vec *= (MAX_TRAJECTORY_LENGTH_PX / trajectory_length)
        trajectory[0:2] = trajectory[2:4] + trajectory_vec
    
@numba.njit
def analyse_lanes(lanes: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Computes length of lanes, and determines vertical lanes as well as whether lanes occur left-sided, right-sided, 
    both-sided or only horizontally.

    Args:
        lanes (np.ndarray): Detected lanes as 2D np.float32 array

    Returns:
        Tuple[np.ndarray, np.ndarray]: Lane lengths and flag array that indicates vertical lanes
    """
    
    lane_count = lanes.shape[0]
    lane_lengths = np.empty((lane_count, ), dtype=np.float32)
    
    vertical_lane_flags = np.zeros((lane_count,), dtype=np.uint8)
    for idx in range(lane_count):
        lane_lengths[idx] = np.linalg.norm(get_lane_vec(lanes[idx]))
        slope = jl._calc_line_coefficients(lanes[idx])[0]
        if abs(slope) >= MIN_VERTICAL_SLOPE:
            vertical_lane_flags[idx] = 1
    
    return lane_lengths, vertical_lane_flags

@numba.njit(cache=True)
def compute_vertical_lane_based_trajectory(lanes: np.ndarray, lane_lengths: np.ndarray, 
                                           vertical_lane_flags: np.ndarray, inv_intrinsic_mtx_33: np.ndarray, 
                                           inv_extrinsic_matrix_44: np.ndarray,) -> np.ndarray:
    """
    Computes trajectory that is based on vertical lanes.

    Args:
        lanes (np.ndarray): Detected lanes
        lane_lengths (np.ndarray): Lane lengths
        vertical_lane_flags (np.ndarray): Flag array that indicates vertical lanes

    Returns:
        np.ndarray: Trajectory based on vertical lanes
    """
    
    lane_count = lanes.shape[0]
    
    # weight lanes by length
    vertical_lane_lengths_sum = lane_lengths[vertical_lane_flags > 0].sum()
    lane_length_divisors = np.ones((lane_count,), dtype=np.float32)
    lane_length_divisors[vertical_lane_flags > 0] = vertical_lane_lengths_sum
    lane_weights = lane_lengths / lane_length_divisors
    
    # weight lanes by distance from car
    distances = np.inf * np.ones((lane_count,), dtype=np.float32)
    for idx, lane in enumerate(lanes):
        if vertical_lane_flags[idx] == 0:
            # skip horizontal lines
            continue
        
        if not np.any(lane):
            # skip empty lines
            continue
        
        # compute distance for bottom point of line
        point_f_world = _calc_xy0_camera_center(lanes[idx, 2:], inv_intrinsic_mtx_33, inv_extrinsic_matrix_44)
        distance = point_f_world[1]
        if distance < DISTANCE_TO_STEER:
            distances[idx] = distance
            
    distances = 1 / distances
    
    if np.any(distances):
        distance_sum = distances.sum()
        distances /= distance_sum

    lane_weights[vertical_lane_flags] *= LENGTH_WEIGTH
    lane_weights[vertical_lane_flags] += DISTANCE_WEIGTH * distances[vertical_lane_flags]
    
    trajectory = (lanes[vertical_lane_flags > 0].T @ lane_weights[vertical_lane_flags > 0])

    return trajectory

@numba.njit(cache=True)
def compute_horizontal_lane_based_trajectory(lanes: np.ndarray, lane_lengths: np.ndarray, inv_intrinsic_mtx_33: np.ndarray, inv_extrinsic_matrix_44: np.ndarray, roi_height: int, image_width: int,
                                             vertical_lane_flags: np.ndarray) -> np.ndarray:
    """
    Computes trajectory that is based on horizontal lanes.

    Args:
        lanes (np.ndarray): Detected lanes
        lane_lengths (np.ndarray): Lane lengths
        inv_intrinsic_mtx_33(nd.array): inverted intrinsic camera matrix of shape 3 x 3
        inv_extrinsic_matrix_44(nd.array): inverted extrinsic camera matrix of shape 4 x 4
        roi_height(int): height of roi in pixel
        image_width(int): initial width of image
        vertical_lane_flags (np.ndarray): Flag array that indicates vertical lanes

    Returns:
        np.ndarray: Trajectory based on horizontal lanes
    """
    
    lane_count = lanes.shape[0]
    horizontal_lane_distances = np.inf * np.ones((lane_count, ), dtype=np.float32)
    for idx in range(lane_count):
        if vertical_lane_flags[idx]:
            continue
        lane = lanes[idx]
        
        if not np.any(lane):
            # skip empty lines
            continue
        
        y_center = np.mean(lane[LANE_Y_MASK > 0])
        center_point = np.array([image_width, y_center + roi_height], dtype=np.float32)
        # transform from camera frame to world frame
        center_point_wf = _calc_xy0_camera_center(center_point, inv_intrinsic_mtx_33, inv_extrinsic_matrix_44)
        distance = center_point_wf[1]
        horizontal_lane_distances[idx] = distance if lane_lengths[idx] > MIN_HORIZONTAL_LENGTH_PX and distance < 0.4 else np.inf
    
    closest_horizontal_lane_distance = np.min(horizontal_lane_distances)
    if closest_horizontal_lane_distance == np.inf or closest_horizontal_lane_distance < DISTANCE_TO_STOP:
        return np.array([0, 0, 0, 0], dtype=np.float32)
    trajectory_length = int(PSEUDO_METERS_TO_PX * closest_horizontal_lane_distance)
    return np.array([0, 0, 0, trajectory_length], np.float32)

@numba.njit
def fuse_lane_based_trajectories(vertical_lane_based_trajectory: np.ndarray, 
                                 horizontal_lane_based_trajectory: np.ndarray) -> np.ndarray:
    """
    Fuses vertical and horizontal lane based trajectories.

    Args:
        vertical_lane_based_trajectory (np.ndarray): Trajectory based on vertical lanes
        horizontal_lane_based_trajectory (np.ndarray): Trajectory based on horizontal lanes

    Returns:
        np.ndarray: Fused trajectory based on lanes
    """
    vertical_lane_based_trajectory_slope = jl._calc_line_coefficients(
        vertical_lane_based_trajectory.astype(np.float32))[0]
    # if vertical lane based trajectory is approximately vertical, only consider horizontal lane based trajectory
    if abs(vertical_lane_based_trajectory_slope) >= MIN_VERTICAL_TRAJECTORY_SLOPE:
        if np.linalg.norm(get_lane_vec(horizontal_lane_based_trajectory)) > 0:
            return horizontal_lane_based_trajectory
        else:
            return vertical_lane_based_trajectory
    # otherwise, horizontal lane based trajectory is misleading 
    else:
        if np.linalg.norm(get_lane_vec(vertical_lane_based_trajectory)) > 0:
            return vertical_lane_based_trajectory
        else:
            return horizontal_lane_based_trajectory

@numba.njit
def compute_lane_based_trajectory_vector(lanes: np.ndarray, inv_intrinsic_mtx_33: np.ndarray, inv_extrinsic_matrix_44: np.ndarray, img_width: int, roi_height: int) -> np.ndarray:
    """
    Interface for computation of lane based trajectory..

    Args:
        lanes (np.ndarray): Detected lanes
        inv_intrinsic_mtx_33(nd.array): inverted intrinsic camera matrix of shape 3 x 3
        inv_extrinsic_matrix_44(nd.array): inverted extrinsic camera matrix of shape 4 x 4
        img_width(int): initial image width
        roi_height(int): height of roi in pixels

    Returns:
        np.ndarray: Trajectory vector based on vertical and horizontal
    """
    lanes_float = lanes.astype(np.float32)
    lane_lengths, vertical_lane_flags = analyse_lanes(lanes_float)
    
    if np.any(vertical_lane_flags):
        vertical_lane_based_trajectory = compute_vertical_lane_based_trajectory(lanes_float, lane_lengths, 
                                                                                vertical_lane_flags, inv_intrinsic_mtx_33, inv_extrinsic_matrix_44)
    else:
        vertical_lane_based_trajectory = np.zeros((4,), dtype=np.float32)   
    clip_lane_based_trajectory(vertical_lane_based_trajectory)
    
    if np.any(vertical_lane_flags == 0):
        horizontal_lane_based_trajectory = compute_horizontal_lane_based_trajectory(
            lanes_float, lane_lengths, inv_intrinsic_mtx_33, inv_extrinsic_matrix_44, roi_height, img_width, vertical_lane_flags)
    else:
        horizontal_lane_based_trajectory = np.zeros((4,), dtype=np.float32)
    clip_lane_based_trajectory(horizontal_lane_based_trajectory)
    
    lane_based_trajectory = fuse_lane_based_trajectories(vertical_lane_based_trajectory, 
                                                         horizontal_lane_based_trajectory)

    return lane_based_trajectory.astype(np.uint16)

@numba.njit
def line_to_trajectory_components(line_vector: np.ndarray) -> np.ndarray:
    """Compute the length and the slope of the direction vector.

    Args:
        trajectory_vector (np.ndarray): direction vector for trajectory (direction indicating the steering
                                        angle, length indicating the speed)

    Returns:
        np.ndarray: array containing length and slope of direction vector
    """
    dy = line_vector[3] - line_vector[1]
    dx = line_vector[2] - line_vector[0]
    
    # invert dx because points are sorted by y values -> correct slope
    dx *= -1
    
    if dx == 0:
        slope = np.inf
    else:
        slope = dy / dx
    
    length = np.sqrt((dy ** 2 + dx ** 2))
    
    return np.array([slope, length], dtype=np.float32)

@numba.njit
def compute_lwma(arr: np.ndarray, weight_matrix: np.ndarray) -> np.ndarray:
    """Computes linear weighted moving average (LWMA).

    Args:
        arr (np.ndarray): input array (n x m)
        weight_matrix (np.ndarray): weight matrix (u x u: u is maximal number of elements)

    Returns:
        np.ndarray: linear weighted moving averaged elements
    """
    element_count = arr.shape[0]
    return arr.astype(np.float32).T @ weight_matrix[element_count-1, :element_count]

@numba.njit
def create_weight_mat(length: int) -> np.ndarray:
    """Generates matrix of weights for linear weighted moving average computation.

    Args:
        length (int): maximal number of elements

    Returns:
        np.ndarray: weights as length x length matrix
    """
    weights_mat = np.zeros((length, length), dtype=np.float32)
    for idx_outer in range(length):
        n = idx_outer + 1
        for idx_inner in range(n):
            weights_mat[idx_outer, idx_inner] = 2 * (idx_inner + 1) / (n * (n + 1))
    return weights_mat
    

scenarios = [("./img/line1.png", "Line 1"), ("./img/line2.png", "Line 2"), ("./img/curve1.png", "Curve 1"), ("./img/curve2.png", "Curve 2")]
scenarios += [("./img/horizontal1.png", "Horizontal 1"), ("./img/horizontal2.png", "Horizontal 2")]
"""scenarios = []
for id in range(10):
    scenarios += [("./img/nearest%04d.png" % id, "Horizontal_%04d" % id)]"""

def performance_eval() -> None:
    repetitions = 100
    for filename, name in scenarios:
        img = cv2.imread(filename)
        camera_transformer = CameraTransformer(img.shape[1], img.shape[0])
        blur, blur_avg, blur_std = timing_wrapper(partial(cv2.GaussianBlur, img, (3,3), 0), repetitions)
        roi, roi_avg, roi_std = timing_wrapper(partial(filter_roi, blur), repetitions)
        edges, edges_avg, edges_std = timing_wrapper(partial(filter_edges, roi), repetitions)
        #cv2.imwrite(name+"_edges.png", edges)
        line_segments, ls_avg, ls_std = timing_wrapper(partial(detect_line_segments, edges), repetitions)
        lanes, lanes_avg, lanes_std = timing_wrapper(partial(jl._join, line_segments, img.shape[1]), repetitions)
        lanes_2, lanes_avg_2, lanes_std_2 = timing_wrapper(partial(correct_lanes_offset, lanes), repetitions)
        traj_lane, traj_lane_avg, traj_lane_std = timing_wrapper(partial(compute_lane_based_trajectory_vector, lanes_2, camera_transformer.inv_intrinsic_cam_matrix_33, camera_transformer.inv_extrinsic_cam_matrix_44, img.shape[1], camera_transformer.roi[0]), repetitions)

        print(name + " - Blur: %.3f ms +- %.3f" % (blur_avg * 1000, blur_std * 1000))
        print(name + " - Roi: %.3f ms +- %.3f" % (roi_avg * 1000, roi_std * 1000)) 
        print(name + " - Edge: %.3f ms +- %.3f" % (edges_avg * 1000, edges_std * 1000)) 
        print(name + " - Hough: %.3f ms +- %.3f" % (ls_avg * 1000, ls_std * 1000))
        print(name + " - Join with grouping: %.3f ms +- %.3f" % (lanes_avg*1000, lanes_std*1000))
        print(name + " - Offset: %.3f ms +- %.3f" % (lanes_avg_2*1000, lanes_std_2*1000))
        print(name + " - Trajectory: %.3f ms +- %.3f" % (traj_lane_avg*1000, traj_lane_std*1000))
        img_cpy = img.copy()

        #for idx in range(len(line_segments)):
        #    p1 = (line_segments[idx][0], line_segments[idx][1])
        #    p2 = (line_segments[idx][2], line_segments[idx][3])
        #    cv2.line(img, p1, p2, color_maps[idx % len(color_maps)], 3)
            
        #cv2.imshow(name + ": Line Segments", img)

        for idx in range(len(lanes_2)):
            p1 = (lanes_2[idx][0], lanes_2[idx][1])
            p2 = (lanes_2[idx][2], lanes_2[idx][3])
            cv2.line(img_cpy, p1, p2, color_maps[idx % len(color_maps)], 3)
        p1 = (traj_lane[0], traj_lane[1])
        p2 = (traj_lane[2], traj_lane[3])
        cv2.line(img_cpy, p1, p2, (0, 0, 0), 3)

        #cv2.imshow(name + ": Lanes", img_cpy)
        #cv2.waitKey(0)
        cv2.imwrite(name+"_lanes.png", img_cpy)
    
def single_run() -> None:
    for filename, name in scenarios:
        print(filename)
        img = cv2.imread(filename)
        camera_transformer = CameraTransformer(img.shape[1], img.shape[0])
        blur = cv2.GaussianBlur(img, (3,3), 0)
        roi = filter_roi(blur)
        edges = filter_edges(roi)
        cv2.imwrite(name+"_edges.png", edges)
        line_segments = detect_line_segments(edges)
        lanes = jl._join(line_segments, img.shape[1])
        line_segments = correct_lanes_offset(line_segments)
        lanes_2 = correct_lanes_offset(lanes)
        
        count_difference = MAX_DETECABLE_OBJECTS - lanes.shape[0]
        if count_difference > 0:
            lanes_2 = np.vstack((lanes_2, np.zeros((count_difference, 4), dtype=np.uint16)))
        elif count_difference <= 0:
            lanes_2 = lanes_2[:MAX_DETECABLE_OBJECTS]
        
        traj_lane = compute_lane_based_trajectory_vector(lanes_2, camera_transformer.inv_intrinsic_cam_matrix_33, camera_transformer.inv_extrinsic_cam_matrix_44, img.shape[1], camera_transformer.roi[1] + ROI_OFFSET_VEC[1])
        
        img_cpy = img.copy()

        for idx in range(len(line_segments)):
            p1 = (line_segments[idx][0], line_segments[idx][1])
            p2 = (line_segments[idx][2], line_segments[idx][3])
            cv2.line(img, p1, p2, color_maps[idx % len(color_maps)], 3)
            
        cv2.imwrite(name + "_lines.png", img)

        for idx in range(len(lanes_2)):
            p1 = (lanes_2[idx][0], lanes_2[idx][1])
            p2 = (lanes_2[idx][2], lanes_2[idx][3])
            cv2.line(img_cpy, p1, p2, color_maps[idx % len(color_maps)], 3)
        
        if traj_lane[0] == 0 and traj_lane[2] == 0:
            # correct vertical lane 
            p1 = (int(traj_lane[0] + camera_transformer.roi[2]//2), int(camera_transformer.roi[3] - traj_lane[1] - 20))
            p2 = (int(traj_lane[2] + camera_transformer.roi[2]//2), int(camera_transformer.roi[3] - traj_lane[3] - 20))
        else:
            p1 = (traj_lane[0], traj_lane[1])
            p2 = (traj_lane[2], traj_lane[3])
        cv2.line(img_cpy, p1, p2, (0, 0, 0), 3)

        #cv2.imshow(name + ": Lanes", img_cpy)
        #cv2.waitKey(0)
        cv2.imwrite(name+"_bundle.png", img_cpy)

if __name__ == "__main__":
    single_run()