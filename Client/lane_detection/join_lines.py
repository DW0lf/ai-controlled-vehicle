"""Implements a line bundler for detected lines, e.g. with OpenCV's HoughLinesP, 
optimized for street markings.
The functions are compiled to machine code using numba.
Firstly, the lines are separated into two groups: one containing lines, which 
are only in the left half of the image, the other one containing lines which are 
only on the right side of the image. If any line is in both of these halfs, all
lines are processed as a single group. If otherwise lines do not cross the 
middle, the lines are processed in two separate groups.
After separation, the lines in of each group are partially bundled, depending on 
the the slope and the spatial proximity.
Finally the joined lines are returned.

Usage:
    joined_lines = join(line_bundle)
"""

import numpy as np
import numba

# threshold for angle between lanes to be joined
ANGULAR_SIMILARITY_THRESHOLD = 1.5
# threshold for distance between one line and the points of another line
SPATIAL_PROXIMITY_THRESHOLD = 10
# mapping for points of two lines to avoid checking distance between one and each other point
MAP_DISTANCE_IDX_TO_LINE_SEGMENT_IDCS = np.array(((0, 1), (2, 3), (0, 3), (2, 1)))
# minimum slope to be classified as vertical line (also specified in Server/src/network/izmq_receiver.py)
MIN_VERTICAL_SLOPE = 0.2
# threshold between two horizontal lanes to be marked as separate lines
Y_SPATIAL_THRESHOLD_HORIZONTAL_LANES = 10


class LineBundler():
    """Implementes bundling of lines detected by Hough Lines detection."""
    def __init__(self, roi_width: int) -> None:
        """Initialize line bundler.

        Args:
            roi_width (int): width of undistored, cropped image
        """
        self.roi_width = roi_width
    
    def bundle(self, line_segments: np.ndarray) -> np.ndarray:
        """Joins lines specified by starting and end point. Therefore the lists are 
        separated into two groups, one for the left half, one for the right half of 
        the image.

        Args:
            line_segments (np.ndarray): nx4 array containing coordinates of 2 points 
                                        per line
            img_size (Tuple[int, int]): image size as tuple

        Returns:
            np.ndarray: joined lines for both groups
        """     
        return _join(line_segments, self.roi_width)   


@numba.njit
def _sort_line_segment_points(line_coordinates: np.ndarray, sort_by_y: bool) -> None:
    """Sorts a numpy array of 4 elements by the y coordinate.
    Works in-place.

    Args:
        line_coordinates (np.ndarray): array of 4 elements (x0, y0, x1, y1) 
        sort_by_y (bool): Points should be sorted by y?
    """
    if sort_by_y:
        condition = line_coordinates[1] > line_coordinates[3]
    else:
        condition = line_coordinates[0] > line_coordinates[2]
    
    if condition:
        _swaggarator(line_coordinates, 0, 2)
        _swaggarator(line_coordinates, 1, 3)

@numba.njit
def _calc_line_coefficients(line_segment: np.ndarray) -> np.ndarray:
    """Calculates the slope and intercept of a line specified by two points.

    Args:
        line_segment (np.ndarray): array of 4 elements (x0, y0, x1, y1) as np.float32

    Returns:
        np.ndarray: slope and intercept as 2 element array  (1D)
    """
    line_segment = line_segment.astype(np.float32)
    dy = (line_segment[3] - line_segment[1])
    dx = (line_segment[2] - line_segment[0])
    if dx != 0:
        slope = dy / dx
    else:
        slope = 0
    intercept = line_segment[1] - line_segment[0] * slope    
    return np.array((slope, intercept), dtype=np.float32)

@numba.njit
def _swaggarator(array: np.ndarray, idx_1: int, idx_2: int) -> None:
    """Swap two elements in an array. Works in place.

    Args:
        array (np.ndarray): input array
        idx_1 (int): index of first element to swap
        idx_2 (int): index of second element to swap
    """
    array[idx_1], array[idx_2] = array[idx_2], array[idx_1]

@numba.njit
def _get_perpendicular_vec(vec: np.ndarray) -> np.ndarray:
    """Calculate perpendicular 2D vector for a given vector.

    Args:
        vec (np.ndarray): input vector

    Returns:
        np.ndarray: vector perpendicular to input vector
    """
    return np.array([vec[1], -vec[0]], dtype=np.float32)

@numba.njit
def _angular_similar(slope_1: float, slope_2: float) -> bool:
    """Determines if lines with given slopes are similar. The degree of similarity
    can be set by ANGULAR_SIMILARITY_THRESHOLD.

    Args:
        slope_1 (float): slope of first line
        slope_2 (float): slope of second line

    Returns:
        bool: slope similar?
    """
    return -ANGULAR_SIMILARITY_THRESHOLD <= (slope_1-slope_2) <= ANGULAR_SIMILARITY_THRESHOLD

@numba.njit
def _spatial_close_vertical(lines: np.ndarray, direction_matrices: np.ndarray, idx1:int, 
                           idx2:int) -> bool:
    """Checks if at least one limiting point of the second line segment is in the 
    neighorhood of the reference line segment (for vertical lines).

    Args:
        lines (np.ndarray): array containing all line coordinates
        direction_matrices (np.ndarray):  array containing direction matrices for 
                                        all lines
        idx1 (int): index of first line
        idx2 (int): index of second line

    Returns:
        bool: lines spatially close?
    """
    return _point_close_to_lane(lines[idx1], direction_matrices[idx1], lines[idx2, :2]) or \
        _point_close_to_lane(lines[idx1], direction_matrices[idx1], lines[idx2, 2:]) 

@numba.njit
def _calc_y_image_center(line: np.ndarray, slope: np.float32, img_width: int) -> np.uint16:
    """
    Computes y coordinate of the given line's center point in horizontal direction.

    Args:
        line (np.ndarray):  Line coordinates
        img_width (int):    width of image

    Returns:
        np.int16: Y coordinate of the center point
    """
    # calculate y coordinate of intersection of line with left image border
    y_left = line[1] - slope * line[0]
    y_right = line[3] + slope * (img_width - line[2])
    y_arr = np.array([y_left, y_right], dtype=np.float32)
    return np.mean(y_arr)

@numba.njit
def _spatial_close_horizontal(lines: np.ndarray, coefficients_of_lines: np.ndarray, idx1:int, 
                             idx2:int, img_width: int) -> bool:
    """Checks if at least one limiting point of the second line segment is in the 
    neighorhood of the reference line segment (for horizontal lines).

    Args:
        lines (np.ndarray): array containing all line coordinates
        coefficients_of_lines (np.ndarray): array containing coefficients (slope and intercept) for all lines
        idx1 (int): index of first line
        idx2 (int): index of second line
        img_width (int):    width of image

    Returns:
        bool: lines spatially close?
    """
    y_center_line_1 = _calc_y_image_center(lines[idx1], coefficients_of_lines[idx1, 0], img_width)
    y_center_line_2 = _calc_y_image_center(lines[idx2], coefficients_of_lines[idx2, 0], img_width)
    
    return abs(y_center_line_1 - y_center_line_2) < Y_SPATIAL_THRESHOLD_HORIZONTAL_LANES

@numba.njit
def _joinable(lines:np.ndarray, coefficients_of_lines:np.ndarray, direction_matrices:np.ndarray, 
             idx1:np.int, idx2:np.int, img_width: int, vertical: bool) -> bool:
    """Checks if lines are similar. It takes spatial proximity and angular 
    similarity into consideration

    Args:
        lines (np.ndarray): array containing all line coordinates
        coefficients_of_lines (np.ndarray): array containing coefficients (slope
                                        and intercept) for all lines
        direction_matrices (np.ndarray):  array containing direction matrices for 
                                        all lines
        idx1 (int): index of first line
        idx2 (int): index of second line
        img_width (int):    width of image
        vertical(bool): flag for vertical (True) or horizontal (False) lines - for selection
                        of right spatial_close functions

    Returns:
        bool: lines joinable?
    """
    angular_similarity = _angular_similar(coefficients_of_lines[idx1, 0], coefficients_of_lines[idx2, 0])
    if vertical:
        spatial_proximity = _spatial_close_vertical(lines, direction_matrices, idx1, idx2)
    else:
        spatial_proximity = _spatial_close_horizontal(lines, coefficients_of_lines, idx1, idx2, img_width)
    return angular_similarity and spatial_proximity

@numba.njit
def _point_close_to_lane(line: np.ndarray, direction_matrices: np.ndarray, 
                        point:np.ndarray) -> bool:
    """Determines if a given point is close to a line.

    Args:
        line (np.ndarray): points of line to check proximity for 
        direction_matrices (np.ndarray): direction matrix for line 
        point (np.ndarray): point coordinates to check proximity for

    Returns:
        bool: point close to line?
    """
    direction_vector_2 = point - line[:2]
    coeff = np.linalg.solve(direction_matrices[:, :], direction_vector_2)
    
    px_aberration = np.linalg.norm((coeff[0] - 1) * direction_matrices[:, 0])
    
    return (0 <= coeff[0] <= 1 or px_aberration <= SPATIAL_PROXIMITY_THRESHOLD) \
        and abs(coeff[1]) <= SPATIAL_PROXIMITY_THRESHOLD

@numba.njit
def _calc_direction_matrix(lines:np.ndarray, direction_matrices:np.ndarray, 
                          idx:int) -> None:
    """Calculates the direction matrix for a line, which consists of the direction
    vector and the perpendicular vector.

    Args:
        lines (np.ndarray): array containing all line coordinates
        direction_matrices (np.ndarray):  array to store new direction matrix in
        idx1 (int): index of line to determine direction matrix for 
    """
    direction_vec = lines[idx, 2:] - lines[idx, :2]
    
    # compute direction vector that is orthogonal to the line segment and 
    # normalized to length 1
    ortho_direction_vec = _get_perpendicular_vec(direction_vec)
    div = np.linalg.norm(ortho_direction_vec)
    if div != 0:
        ortho_direction_vec /= div
    direction_matrices[idx, :, 0] = direction_vec
    direction_matrices[idx, :, 1] = ortho_direction_vec

@numba.njit
def _join_lines(lines:np.ndarray, idx1:int, idx2:int) -> None:
    """Joins two lines by reusing the two furthermost points. Replaces the first 
    line.

    Args:
        lines (np.ndarray): array containing all line coordinates
        idx1 (int): index of first line to join
        idx2 (int): index of second line to join
    """
    points = np.vstack((lines[idx1, :2], lines[idx1, 2:], lines[idx2, :2], lines[idx2, 2:]))
    distances = np.array([np.linalg.norm(
        points[MAP_DISTANCE_IDX_TO_LINE_SEGMENT_IDCS[idx, 0]] - 
        points[MAP_DISTANCE_IDX_TO_LINE_SEGMENT_IDCS[idx, 1]]) for idx in 
                          range(MAP_DISTANCE_IDX_TO_LINE_SEGMENT_IDCS.shape[0])])
    max_idx = np.argmax(distances)
    joined_line_idx_1, joined_line_idx_2 = MAP_DISTANCE_IDX_TO_LINE_SEGMENT_IDCS[max_idx, :]
    lines[idx1, :2] =  points[joined_line_idx_1]
    lines[idx1, 2:] = points[joined_line_idx_2]

@numba.njit 
def _point_on_right_side(point: np.ndarray, img_width: int) -> bool:
    """Determines the position of the point in horizontal direction. Returns False 
    if point is in the left half of the image. Returns True if point is in the
    right half of the image

    Args:
        point (np.array): point coordinates

    Returns:
        bool: point in left half?
    """
    return point[0] <= img_width // 2

@numba.njit
def _join(line_segments: np.ndarray, img_width: int) -> np.ndarray:
    """Joins lines specified by starting and end point. Therefore the lists are 
    separated into two groups, one for the left half, one for the right half of 
    the image.

    Args:
        line_segments (np.ndarray): nx4 array containing coordinates of 2 points 
                                    per line
        img_width (int):    width of image (the middle of it is used to seperate
                            the vertical lines into two groups)

    Returns:
        np.ndarray: joined lines for both groups
    """
    
    line_segment_count = line_segments.shape[0]
    
    horizontal_group = np.zeros((line_segment_count,), dtype=np.uint8)
    coefficients_of_lines = np.empty((line_segment_count, 2), dtype=np.float32)
    left_group = np.zeros((line_segment_count,), dtype=np.uint8)
    right_group = np.zeros((line_segment_count,), dtype=np.uint8)
    crossing_middle = False 
    for idx in range(line_segment_count):
        point0 = line_segments[idx, :2]
        point1 = line_segments[idx, 2:]

        line_coefficients = _calc_line_coefficients(line_segments[idx])
        coefficients_of_lines[idx, :] = line_coefficients
        if abs(line_coefficients[0]) < MIN_VERTICAL_SLOPE:
            horizontal_group[idx] = 1 
        else:
            if _point_on_right_side(point0, img_width) and _point_on_right_side(point1, img_width):
                right_group[idx] = 1
            elif not _point_on_right_side(point0, img_width) and not _point_on_right_side(point1, img_width):
                left_group[idx] = 1
            else:
                # one line crossing the middle in horizontal direction -> list can not be splitted
                crossing_middle = True
                right_group[idx] = 1
                left_group[idx] = 1
    
    if np.any(horizontal_group):
        horizontal_lanes = _join_group(line_segments[horizontal_group > 0], coefficients_of_lines[horizontal_group > 0],
                                       img_width, False)
    else:
        horizontal_lanes = np.array([[0]], np.uint16)

    if crossing_middle:
        # there is / are line(s) crossing the horizontal middle of image
        left_right_indices = np.logical_or(left_group, right_group)
        vertical_lanes = _join_group(line_segments[left_right_indices > 0], 
                                     coefficients_of_lines[left_right_indices > 0], img_width, True)
        if np.any(horizontal_lanes):
            return np.vstack((vertical_lanes, horizontal_lanes))
        else:
            return vertical_lanes
    else:
        # there are atmost horizontal lines
        if not np.any(left_group) and not np.any(right_group):
            if np.any(horizontal_lanes):
                return horizontal_lanes
            else:
                return np.array([[0]], dtype=np.uint16)
        elif not np.any(left_group):
            # there are only lines on the right side and maybe horizontal lines
            vertical_lanes = _join_group(line_segments[right_group > 0], coefficients_of_lines[right_group > 0], 
                                         img_width, True)  
            if np.any(horizontal_lanes):
                return np.vstack((vertical_lanes, horizontal_lanes))
            else:
                return vertical_lanes
        elif not np.any(right_group):
            # there are only lines on the left side and maybe horizontal lines
            vertical_lanes = _join_group(line_segments[left_group > 0], coefficients_of_lines[left_group > 0], 
                                         img_width, True)  
            if np.any(horizontal_lanes):
                return np.vstack((vertical_lanes, horizontal_lanes))
            else:
                return vertical_lanes
        else:
            # there are lines on the left and right side and maybe horizontal lines
            right_lanes = _join_group(line_segments[right_group > 0], coefficients_of_lines[right_group > 0], 
                                      img_width, True)
            left_lanes = _join_group(line_segments[left_group > 0], coefficients_of_lines[left_group > 0], 
                                     img_width, True)
            if np.any(horizontal_lanes):
                return np.vstack((right_lanes, left_lanes, horizontal_lanes))
            else: 
                return np.vstack((right_lanes, left_lanes))

@numba.njit
def _join_group(line_segments: np.ndarray, coefficients_of_lines: np.ndarray, img_width: int, 
                vertical: bool) -> np.ndarray:
    """Actual joining of lines.

    Args:
        line_segments (np.ndarray): nx4 array containing coordinates of 2 points 
                                    per line
        coefficients_of_lines(np.ndarray): nx2 array containing slope and intercept of lanes
        img_width (int): width of image
        vertical(bool): flag for vertical (True) or horizontal (False) lines - for selection
                        of right spatial_close functions
    Returns:
        np.ndarray: joined lines
    """
    # initialize numpy arrays containing values for all line segments
    count_lines = line_segments.shape[0]
    lines = np.zeros((count_lines, 4), dtype=np.float32)
    not_joined = np.ones(count_lines, dtype=np.uint8)
    direction_matrices = np.zeros((count_lines, 2, 2), dtype=np.float32)
    
    # initialize line segments
    for idx, segment in enumerate(line_segments):
        _sort_line_segment_points(segment, vertical)
        lines[idx, :] = segment
        _calc_direction_matrix(lines, direction_matrices, idx)
    
    # actual joining
    joined_last_iteration = True
    while joined_last_iteration:
        joined_last_iteration = False
        for idx1 in range(count_lines):
            if not not_joined[idx1]:
                continue
            for idx2 in range(count_lines):
                if not not_joined[idx2] or idx1 == idx2:
                    continue

                if _joinable(lines, coefficients_of_lines, direction_matrices, idx1, idx2, img_width, vertical):
                    _join_lines(lines, idx1, idx2)  
                    not_joined[idx2] = 0
                    _calc_direction_matrix(lines, direction_matrices, idx1)
                    coefficients_of_lines[idx1, :] = _calc_line_coefficients(lines[idx1])
                    joined_last_iteration = True
    
    return lines[not_joined > 0, :].astype(np.uint16)
