
import numpy as np
import numba

# ORIENTATION OF WORLD COORDINATE SYSTEM
#   X: pointing to the left (parallel to the camera lense)
#   Y: pointing to the front of the car ("horzontal direction" of camera)
#   Z: pointing downwards

# parameters from calibration
INTRINSIC_CAM_MATRIX_33 = np.genfromtxt("params/scaled_intrinsic_matrix.csv", delimiter=",", dtype=np.float32)
INV_INTRINSIC_CAM_MATRIX_33 = np.linalg.inv(INTRINSIC_CAM_MATRIX_33).astype(np.float32)
INV_INTRINSIC_CAM_MATRIX_34 = np.vstack((INV_INTRINSIC_CAM_MATRIX_33, np.array([[0,0,1]])))
EXTRINSIC_CAM_MATRIX_44 = np.genfromtxt("params/extrinsic_matrix.csv", delimiter=",", dtype=np.float32)
INV_EXTRINSIC_CAM_MATRIX_44 = np.linalg.inv(EXTRINSIC_CAM_MATRIX_44).astype(np.float32)
CAM_F_WORLD = np.array([0.08149753,  0.51926134, -0.16415225], dtype=np.float32) # position of camera in world coordinate system
TRANSLATE_PATTERN_CENTER = np.array([-0.08, -0.06, 0], dtype=np.float32)  # translation vector to set center of pattern as origin
ROT_MATRIX_180_DEG_Z_33 = np.array([[-1, 0, 0], [0, -1, 0], [0, 0, 1]], dtype=np.float32)

@numba.njit
def conv_to_homogenous(point):
    """Convert n-dimensional coordinates the homogenous coordinates (n+1-dimensional)

    Args:
        point(np.array): nx1 array
    
    Returns:
        np.array: point in homogenous coordinates
    """
    point_homogenous = np.hstack((point, np.array([1])))
    return point_homogenous.astype(np.float32)

@numba.njit
def calc_intersection(point1, cam_f_world, plane_normal, plane_point, epsilon=1e-8):
    """Calculates intersection of line from point 1 to point 2 with the 
    specified plane.

    Args:
        point1(np.array): first 3D point (on the line)
        point2(np.array): second 3D point (on the line)
        plane_normal(np.array): normal vector of plane
        plane_point(np.array): any point in the plane
    
    Returns:
        np.array: intersection point between point and plane
    """
    direction_vector = point1 - cam_f_world 
    ndotu = plane_normal.dot(direction_vector)

    if abs(ndotu) < epsilon:
        raise RuntimeError("no intersection or line is within plane")

    w = point1 - plane_point
    si = -plane_normal.dot(w) / ndotu
    Psi = w + si * direction_vector + plane_point
    return Psi

@numba.njit
def _calc_xy0_calib_board_center(point_2d):
    """Calculate intersection of line from camera to point, determined by 
    projection of image coordinates to world space (center of calibration pattern as origin
    of world coordinate system), and the x-y-plane.
    
    Args:
        point_2d(np.array): pixel coordinates of point to project to world
                            coordinate system (pixel coordinates in undistorted, not 
                            already cropped image)
    
    Returns:
        np.array: point in world coordinate system"""
    # convert pixel coordinates to homogenous coordinates
    point_homogenous_f_img = conv_to_homogenous(point_2d)

    # convert homogenous pixel coordinates to homogenous camera coordinates
    point_f_cam = INV_INTRINSIC_CAM_MATRIX_33.dot(point_homogenous_f_img)
    point_homogenous_f_cam =  conv_to_homogenous(point_f_cam)

    # convert homogenous camera coordinates to world coordinates
    point_f_world = INV_EXTRINSIC_CAM_MATRIX_44.dot(point_homogenous_f_cam)
    point_f_world = point_f_world[:-1]

    # calculate intersection between line from camera to determined point and x-y-plane
    plane_normal = np.array([0,0,1], dtype=np.float32)
    plane_point = np.array([0,0,0], dtype=np.float32)
    point_f_world = calc_intersection(point_f_world, CAM_F_WORLD, plane_normal, plane_point)

    # translate points to set center of pattern size as origin of world coordinate system
    point_f_world += TRANSLATE_PATTERN_CENTER

    # rotate around z axis by 180 to ensure positive x axis pointing to the front
    point_f_world = point_f_world @ ROT_MATRIX_180_DEG_Z_33
    return point_f_world

@numba.njit
def calc_xy0_camera_center(point_2d: np.ndarray) -> np.ndarray:
    """Calculate intersection of line from camera to point, determined by 
    projection of image coordinates to world space (point on ground beneath the camera as 
    origin of world coordinate system), and the x-y-plane.
    
    Args:
        point_2d(np.array): pixel coordinates of point to project to world
                            coordinate system (pixel coordinates in undistorted, not 
                            already cropped image)
    
    Returns:
        np.array: point in world coordinate system"""
    return _calc_xy0_calib_board_center(point_2d) + np.array([0, CAM_F_WORLD[1], 0])

def test_projection_to_xy_plane():
    """Tests the projection of pixel coordinates from the undistorted image to
    the world coordinate system."""
    bottom_to_center_f_world = _calc_xy0_calib_board_center(np.array([419, 264]))
    print("very bottom right (undistorted)", bottom_to_center_f_world)
    np.testing.assert_array_almost_equal(bottom_to_center_f_world, 
        np.array([-0.0797743,  -0.05805401, 0.        ]))
    bottom_to_center_f_world = _calc_xy0_calib_board_center(np.array([205, 265]))
    print("very bottom left (undistorted)", bottom_to_center_f_world)
    np.testing.assert_array_almost_equal(bottom_to_center_f_world, 
        np.array([0.08043626,  -0.05976555,  0.        ]))
    bottom_to_center_f_world = _calc_xy0_calib_board_center(np.array([311, 237]))
    print("center (undistorted)", bottom_to_center_f_world)
    np.testing.assert_array_almost_equal(bottom_to_center_f_world, 
        np.array([0.00098267, 0.00206307,  0.        ]))
    bottom_to_center_f_world = _calc_xy0_calib_board_center(np.array([396, 216]))
    print("very top right (undistorted)", bottom_to_center_f_world)
    np.testing.assert_array_almost_equal(bottom_to_center_f_world, 
        np.array([-0.0797743,  0.05805401, 0.        ]))
    bottom_to_center_f_world = _calc_xy0_calib_board_center(np.array([229, 217]))
    print("very top left (undistorted)", bottom_to_center_f_world)
    #np.testing.assert_array_almost_equal(bottom_to_center_f_world, 
    #    np.array([0.08043626,  -0.05976555,  0.        ]))
    bottom_to_center_f_world = _calc_xy0_calib_board_center(np.array([312, 218]))
    print("very top center (undistorted)", bottom_to_center_f_world)
    bottom_to_center_f_world = _calc_xy0_calib_board_center(np.array([312, 213.5], np.float32))
    print("horizontal lane center", bottom_to_center_f_world)

if __name__ == "__main__":
    test_projection_to_xy_plane()
