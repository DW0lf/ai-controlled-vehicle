#!/usr/bin/env python3

"""Implements LaneDetector for Jetson."""

import time
from typing import Any, List, Tuple

import cv2
import numpy as np

from image_processing.join_lines import LineBundler
from process_wrapper import ProcessWrapper


class LaneDetector(ProcessWrapper):
    """Class for lane detection, which is executed in a separate process."""
    
    def __init__(self, shared_image_data_ptr: Any, shared_lane_data_ptr: Any, shared_manual_control_data_ptr: Any, 
                 image_procession_event: Any, data_copy_event: Any, data_sending_event: Any, 
                 control_application_event: Any, image_capture_event: Any, stop_event: Any, emergency_stop_event: Any, 
                 image_procession_barrier: Any, startup_barrier: Any, img_roi: Tuple[int, int, int, int], 
                 max_detectable_lanes: int):
        """Initializes LaneDetector.

        Args:
            shared_image_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image
            shared_lane_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data
            shared_manual_control_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for manual 
            control flag
            image_procession_event (multiprocessing.Event): Event for image procession initiation
            data_copy_event (multiprocessing.Event): Event for copying data
            data_sending_event (multiprocessing.Event): Event for data transmission initiation
            control_application_event (multiprocessing.Event): Event for control application initiation
            image_capture_event (multiprocessing.Event): Event for image capture initiation
            emergency_stop_event (multiprocessing.Event): Event for emergency stop signal
            image_procession_barrier (multiprocessing.Barrier): Barrier that synchronizes lane detection and object 
            detection
            startup_barrier (multiprocessing.Barrier): Barrier that synchronizes startup of all processes
            img_roi (Tuple[int, int, int, int]): Shape of image's region of interest (x, y, w, h)
            max_detectable_lanes (int): Maximum number of lanes that can be detected at a time
        """

        self.img_roi = img_roi
        self.max_detectable_lanes = max_detectable_lanes
        self.line_bundler = LineBundler(self.img_roi[2])

        # init events and barriers
        self.image_procession_event = image_procession_event
        self.data_copy_event = data_copy_event
        self.data_sending_event = data_sending_event
        self.control_application_event = control_application_event
        self.image_capture_event = image_capture_event
        self.stop_event = stop_event
        self.emergency_stop_event = emergency_stop_event
        self.image_procession_barrier = image_procession_barrier
        self.startup_barrier = startup_barrier
        
        # init process
        shared_memory_pointers = (shared_image_data_ptr, shared_lane_data_ptr, shared_manual_control_data_ptr)
        super().__init__(shared_memory_pointers, emergency_stop_event)
        
    def _detect(self, shared_img_data: np.ndarray) -> np.ndarray:
        """
        Detects lanes in given image.

        Args:
            shared_img_data (np.ndarray): OpenCV image

        Returns:
            np.ndarray[n, 4][np.uint16]: Lane data as [[x00, y00, x01, y01], ..., [xn0, yn0, xn1, yn1]] array
        """
        
        blurred_img = cv2.GaussianBlur(shared_img_data, self.GAUSSIAN_KERNEL_SIZE, self.GAUSSIAN_KERNEL_STD)
        roi = self._filter_roi(blurred_img)
        edges = self._filter_edges(roi)
        line_segments = self._detect_line_segments(edges)
        lanes = self.line_bundler.bundle(line_segments)
        lanes = self._correct_lane_offset(lanes)
        
        count_difference = self.max_detectable_lanes - lanes.shape[0]
        if count_difference > 0:
            return np.vstack((lanes, np.zeros((count_difference, 4), dtype=np.uint16)))
        elif count_difference <= 0:
            return lanes[:self.max_detectable_lanes]
    
    def _detect_line_segments(self, image: np.ndarray) -> List[np.ndarray]:
        """
        Detects line segments in the image using Hough Transform algorithm.

        Args:
            image (np.ndarray): OpenCV input image

        Returns:
            List[np.ndarray[4]]: List of detected line segments (as [x0, y0, x1, y1] arrays)
        """
        hough_lines = cv2.HoughLinesP(image, self.HOUGH_RHO, self.HOUGH_MIN_ANGLE, self.HOUGH_MIN_VOTES, 
                                          minLineLength=self.HOUGH_MIN_LINE_LENGTH, maxLineGap=self.HOUGH_MAX_LINE_GAP)
        if hough_lines is None:
            return np.zeros((1,4), dtype=np.uint16)
        else:
            return np.squeeze(hough_lines, 1).astype(np.uint16)
        
    def _filter_edges(self, image: np.ndarray) -> np.ndarray:
        """
        Filter edges in the image using Canny algorithm.

        Args:
            image (np.ndarray): OpenCV input image

        Returns:
            np.ndarray: OpenCV image with filtered edges
        """
        
        diff_rg = (image[:, :, 1] + self.COLOR_OFFSET) - image[:, :, 2]
        avg = np.average(diff_rg) * self.AVERAGE_MULTIPLICATOR

        gray = (diff_rg > avg).astype(np.uint8) * 255
        gray = cv2.morphologyEx(gray, cv2.MORPH_RECT, self.CLOSING_KERNEL, iterations=1)
    
        # filter edges using Canny algorithm
        return cv2.Canny(gray, self.CANNY_THRESHOLD_1, self.CANNY_THRESHOLD_2)
    
    def _filter_roi(self, image: np.ndarray) -> np.ndarray:
        """
        Filters region of interest (roi) in the image by cropping

        Args:
            image (np.ndarray): OpenCV input image

        Returns:
            np.ndarray: OpenCV image with filtered roi
        """
        
        return image[self.ROI_SLICE_Y, self.ROI_SLICE_X]
    
    def _correct_lane_offset(self, lanes: np.ndarray) -> np.ndarray:
        """
        Corrects offset on lanes that is caused by former image cropping.

        Args:
            lanes (np.ndarray): Detected lanes

        Returns:
            np.ndarray: Detected lanes with corrected offset
        """
        
        return lanes + self.OFFSET_VEC
    
    def _init(self):
        """Initializes LaneDetector process specifically."""
                
        # Gaussian blur used initially to cancel out noise
        self.GAUSSIAN_KERNEL_SIZE = (3, 3)
        self.GAUSSIAN_KERNEL_STD = 0
        
        # closing kernel used to close gaps before canny edge detection
        self.CLOSING_KERNEL = cv2.getStructuringElement(cv2.MORPH_CROSS, (5, 5))
        self.CANNY_THRESHOLD_1 = 50
        self.CANNY_THRESHOLD_2 = 150
        
        # take bottom half of the image as roi
        self.ROI_SLICE_X = slice(1, -1)
        self.ROI_SLICE_Y = slice(self.img_roi[3]//3, None)
        self.OFFSET_VEC = np.array([self.ROI_SLICE_X.start, self.ROI_SLICE_Y.start, self.ROI_SLICE_X.start, 
                                    self.ROI_SLICE_Y.start], dtype=np.uint16)

        
        # distance precision in pixel, i.e. 1 pixel
        self.HOUGH_RHO = 3
        # angular precision in radian, i.e. 1 degree
        self.HOUGH_MIN_ANGLE = np.pi / 180
        # number of votes needed to be considered a line segment
        # if a line has more votes, Hough Transform considers them to be more likely to have detected a line segment
        self.HOUGH_MIN_VOTES = 50
        # minimum length of the line segment in pixels
        # Hough Transform won’t return any line segments shorter than this minimum length
        self.HOUGH_MIN_LINE_LENGTH = 20
        # maximum in pixels that two line segments that can be separated and still be considered a single line segment
        # for example, if we had dashed lane markers, by specifying a reasonable max line gap, Hough Transform will 
        # consider the entire dashed lane line as one straight line, which is desirable
        self.HOUGH_MAX_LINE_GAP = 5
        # value is added to the green color channel before binarization
        self.COLOR_OFFSET = 54
        # multiplicator for average of the green color channel -> result is threshold for binarization
        self.AVERAGE_MULTIPLICATOR = 1.25

    def _run(self, shared_image_data_ptr: Any, shared_lane_data_ptr: Any, shared_manual_control_data_ptr: Any):
        """
        Cyclic worker method, detects lanes and stores lane data in shared memory.

        Args:
            shared_image_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image
            shared_lane_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data
            shared_manual_control_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for manual 
            control flag
        """
        
        lanes = None

        # create numpy arrays, representing views onto shared memory
        shared_img_data = np.frombuffer(shared_image_data_ptr, dtype=np.uint8).reshape(self.img_roi[3], self.img_roi[2], 
                                                                                       3)
        shared_lane_data = np.frombuffer(shared_lane_data_ptr, dtype=np.uint16).reshape((self.max_detectable_lanes, 4))

        # wait until all processes are ready on startup
        self.logger.debug("Waiting for Startup...")
        self.startup_barrier.wait()

        while True:
            # wait until new image has been captured
            self.logger.debug("Waiting for E_2...")
            self.image_procession_event.wait()
            self.logger.debug("E_2 set. Detecting lanes...")
            
            # wait, if stop signal was given by remote
            if not self.stop_event.is_set():
                self.logger.info("Stopped until remote gives signal to proceed.")
            self.stop_event.wait() 

            # detect lanes and store lane data in shared memory
            lanes = self._detect(shared_img_data)
            np.copyto(shared_lane_data, lanes)

            # reset waiting event (must happen before barrier, so that object detector cannot enter the loop a second 
            # time in the same cycle)
            self.image_procession_event.clear()
            self.logger.debug("Lanes detected. E_2 cleared. Waiting for Barrier...")
            # wait until object detection has been finished as well
            self.image_procession_barrier.wait()
            # reset barrier
            self.image_procession_barrier.reset()
            self.logger.debug("Barrier passed and reset.")
            
            # check if manual control 
            if shared_manual_control_data_ptr.value:
                self.logger.debug("Manual control enabled.")
                # if transmitter process is busy -> notify camera process
                if self.data_sending_event.is_set():
                    self.logger.debug("Transmitter busy. Notifying Camera...")
                    self.image_capture_event.set()
                    self.logger.debug("Set E_1.")
                # if transmitter process is ready -> notify copy worker process
                else:
                    self.logger.debug("Transmitter ready. Notifying Copy Worker...")
                    self.data_copy_event.set()
                    self.logger.debug("Set E_4.")
            # otherwise, notify CarController process
            else:
                self.control_application_event.set()
                self.logger.debug("Set E_3.")

    def _on_termination(self):
        """Is invoked on process termination."""
        
        # TODO: Implement behavior on process termination 
        pass