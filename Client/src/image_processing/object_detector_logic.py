#!/usr/bin/env python3
import numba
import numpy as np
import cv2

IMG_HEIGHT_CROPPED = 30
# RANSAC
MINIMAL_Y_DIFF = 40 # minimum length of line segment to return by ransac
MAX_X_DIFF = 20 # maximum distance between x values of line points returned by ransac

# maximize line and updating point cloud
Y_GAP_THRESH = 6    # maximal gap between two pixels to be joined to one line (number of black lines)
X_WINDOW = 4        # window size in x-direction, this window is moved upwards 
                    # and downwards to detect other pixels belonging to the line 
                    # (total size in x-direction 2*X_WINDOW+1) 
                    # 
                    # It also defines the padding area around a line in which the point cloud
                    # get's cleaned up 
MINIMAL_LINE_HEIGHT = 50 # minimal height in px to detect a sign

# determine bounding boxes to return
BOUNDING_BOX_Y_OFFSET = 0.15 # % of line length which will be deducted from line end
BOUNDING_BOX_SIZE = 0.7 # % of line length 

ITERATIONS_PER_OBJECT = 2

@numba.njit(cache=True)
def make_point_cloud(binary_img : np.ndarray) -> np.ndarray:
    """Generates point cloud from binary image. All elements is point 
    cloud constist of x,y coordinates of active pixels.
    
    Args:
        binary_img(np.ndarray): binary image to detect white pixels in
    
    Returns:
        np.ndarray: nx2 numpy array containing y and x coordinates of active pixels
    """
    y_val, x_val = np.nonzero(binary_img)
    cloud = np.column_stack((y_val,x_val))
    cloud = cloud[cloud[:, 1].argsort()] # sort cloud after x
    return cloud

@numba.njit(cache=True)
def points_valid(point0 : np.ndarray, point1 : np.ndarray) -> bool:
    """Calculates if the two points are valid to find a vertical line.

    Args:
        point0 (np.ndarray): first point
        point1 (np.ndarray): second point

    Returns:
        bool: are two points nearly vertical and is the distance between them above \"MINIMAL_Y_DIFF\"
    """
    # compute difference of x,y values
    diff = np.abs(point0-point1)

    return (diff[0] > MINIMAL_Y_DIFF) and (diff[1] < MAX_X_DIFF)

@numba.njit(cache=True)
def random_vertical_line(point_cloud : np.ndarray) -> np.ndarray:
    """Find two random, nearly vertical points with RANSAC algorithm.

    Args:
        point_cloud (np.ndarray): array with pixel coordinates (y_val, x_val) to choose from

    Returns:
        np.ndarray: two points with the smaller y as the second point, e.g. (60,120,80,110) (y_start, x_start, y_end, x_end)
    """    
    max_iterations = point_cloud.shape[0] - 1
    while max_iterations:
        max_iterations -= 1
        # receive two random indices from point cloud
        indices = np.random.choice(point_cloud.shape[0], 2, replace=False)
        
        p0 = point_cloud[indices[0]]
        p1 = point_cloud[indices[1]]
        if points_valid(p0,p1):
            break
    
    # place smaller y at the end, important for \"maximize_line\"
    if p1[0] - p0[0] < 0:
        p0, p1 = p1, p0
    if max_iterations > 0:
        return np.array([p1[0], p1[1], p0[0], p0[1]], dtype=np.int16)
    else:
        # int because slope calc in maximize line... see main method
        return np.zeros((4,), dtype=np.int16) 

@numba.njit(cache=True)
def maximize_line(line : np.ndarray, binary_img : np.ndarray) -> np.ndarray:
    """Maximizes line segment by moving start point downwards and searching for 
    end point by moving sliding window upwards .

    Args:
        line (np.ndarray): one line defined by 4 points with structure (y_start, x_start, y_end, x_end)
        binary_img (np.ndarray): binary image as numpy array

    Returns:
        np.ndarray: maximized line segments or numpy array containing zeros
    """
    # smaller y is end point (at the top)    
    line_start = line[:2] # bottom point
    line_end = line[2:] # top point

    # how much pixel in x direction per y
    # negative if start point is left of end point (smaller x)
    inv_slope = (line_start[1] - line_end[1]) / (line_start[0] - line_end[0])
    
    # move start point (bottom) downwards to check for gaps
    gap_counter = Y_GAP_THRESH
    old_line_start = line_start # save line start for later
    y,x_mid = line_start
    x_new = x_mid
    
    # TODO: check y is in image (y<img.height)
    while gap_counter > 0:
        y += 1
        x_mid = x_mid + inv_slope
        x_mid_int = int(np.rint(x_mid))
        
        # the following condition and loop build the sliding window with a width 
        # of X_WINDOW*2+1 pixels and move it downwards
        # e.g. if X_WINDOW is equal to 2 than at the middle element (index 2) of 
        # the binary image is checked at first, afterwards at index 1 and 3 and 
        # finally at index 0 and 5 
        if binary_img[y, x_mid_int]:
            # check middle (most likely positon) of sliding window
            x_new = x_mid_int
            gap_counter = Y_GAP_THRESH
        else:
            # check other positions of sliding window
            for x_win in range(1, X_WINDOW):
                if binary_img[y, x_mid_int + x_win]:
                    # active pixel found
                    x_new = x_mid_int + x_win
                    gap_counter = Y_GAP_THRESH
                    break
                if binary_img[y, x_mid_int - x_win]:
                    # active pixel found 
                    x_new = x_mid_int - x_win
                    gap_counter = Y_GAP_THRESH
                    break
                
        gap_counter -= 1
    
    # update line start with correct coordinates 
    line_start = y - Y_GAP_THRESH, x_new

    # traverse upwards to find and maximize end point
    gap_counter = Y_GAP_THRESH
    y, x_mid = old_line_start
    
    while gap_counter > 0:
        y -= 1
        x_mid = x_mid - inv_slope
        x_mid_int = int(np.rint(x_mid))
        
        # refer to above explanation
        if binary_img[y, x_mid_int]:
            x_new = x_mid_int
            gap_counter = Y_GAP_THRESH
        else:
            for x_win in range(1, X_WINDOW):
                if binary_img[y, x_mid_int + x_win]:
                    # active pixel found
                    x_new = x_mid_int + x_win
                    gap_counter = Y_GAP_THRESH
                    break
                if binary_img[y, x_mid_int - x_win]:
                    # active pixel found
                    x_new = x_mid_int - x_win
                    gap_counter = Y_GAP_THRESH
                    break

        gap_counter -= 1

    y += Y_GAP_THRESH
    # if y is small enough line end was met
    if y <= line_end[0] or (line_start[0] - y) > MINIMAL_LINE_HEIGHT:
        # return line if it includes initial start and end point
        line_end = y, x_new
        return np.array([line_start[0], line_start[1], line_end[0], line_end[1]], dtype=np.int16)
    else:
        # return arrays of zeros if initial start and end points are not included
        return np.zeros((4,), dtype=np.int16) 

@numba.njit(cache=True)
def calculate_bounding_boxes(lines : np.ndarray, bounding_boxes : np.ndarray, img_width : int):
    """Computes bounding boxes at line endings.
    
    Args:
        lines(np.ndarray): Filtered lines with following structure (start_y, start_x, end_y, end_x)
        bounding_boxes(np.ndarray): Bounding box array ([[Enum_node_kind,y0,x0,y1,x1]...], np.int16)
        img_width(int): Width of the image
    Returns:
        np.ndarray: shape[lines.shape,4] with left lower corner (y,x) and right upper corner (y,x)
    """
    for idx, line in enumerate(lines):
        if not np.any(line):
            continue
        length = line[0] - line[2]
        # use line end only!
        p0_y = line[2] + int(length*BOUNDING_BOX_Y_OFFSET) + IMG_HEIGHT_CROPPED
        p0_x = line[3] - int(length*BOUNDING_BOX_SIZE) // 2
        p1_y = p0_y - int(length*BOUNDING_BOX_SIZE)
        p1_x = p0_x + int(length*BOUNDING_BOX_SIZE)
        # skip bounding boxes which exceed image borders
        # only necessary for upper border and left / right image border
        if p1_y < 0 or p0_x < 0 or p1_x > img_width:
            continue

        bounding_boxes[idx,1:] = p0_y, p0_x, p1_y, p1_x

@numba.njit(cache=True)
def update_point_cloud(cloud : np.ndarray, line : np.ndarray) -> np.ndarray:
    """Updates point cloud by removing all points which coordinates are inside 
    a rectangular window, specified by a found line.
    
    Args:
        cloud (np.ndarray): point_cloud sorted after y
        line (np.ndarray): array which describes a line (y_start,x_start, y_end,x_end)
    
    Returns:
        np.ndarray: new point cloud
    """
    # if line is empty (0 only) then there is nothing to do
    if not np.any(line):
        return cloud
    
    line_start = line[:2].copy()
    line_end = line[2:].copy()
    
    # create padded rectangle around line 
    if line_start[1] > line_end[1]:
        line_start[1] = line_start[1] + X_WINDOW
        line_end[1] = line_end[1] - X_WINDOW
        idx_start = np.searchsorted(cloud[:,1], line_end[1], side='left')
        idx_end = np.searchsorted(cloud[:,1], line_start[1], side='right')
    elif line_start[1] < line_end[1]:
        line_start[1] = line_start[1] - X_WINDOW
        line_end[1] = line_end[1] + X_WINDOW
        idx_start = np.searchsorted(cloud[:,1], line_start[1], side='left')
        idx_end = np.searchsorted(cloud[:,1], line_end[1], side='right')
    # x values the same --> bigger Padding Area
    else:
        line_start[1] = line_start[1] - X_WINDOW*2
        line_end[1] = line_end[1] + X_WINDOW*2
        idx_start = np.searchsorted(cloud[:,1], line_start[1], side='left')
        idx_end = np.searchsorted(cloud[:,1], line_end[1], side='right')

    mask = np.ones((cloud.shape[0],), dtype=np.uint16)

    # delete all points in cloud columnwise
    for idx in range(idx_start, idx_end):
        mask[idx] = 0

    return cloud[mask > 0]

@numba.njit(cache=True)
def remove_empty_lines(lines: np.ndarray, target_size : int) -> np.ndarray:
        """Remove empty lines from input matrix. 

        Args:
            lines (np.ndarray): (2 * max_object_count, 4) array
            target_size (int): target size for return array

        Returns:
            np.ndarray: (max_object_count, 4) array
        """
        rtrn_lines = np.zeros((target_size, 4), dtype=np.int16)
        cnt = 0
        for line in lines:
            if not np.any(line):
                continue
            else:
                rtrn_lines[cnt, :] = line
                cnt += 1
        
        return rtrn_lines

@numba.njit(cache=True)
def find_vertical_lines(thresh : np.ndarray, max_detected_objects : int) -> np.ndarray:
    """Finds at most \"max_detected_objects\" lines

    Args:
        thresh (np.ndarray): image with Sobel and threshold applied
        max_detected_objects (int): max numer of objects to detect

    Returns:
        np.ndarray: \"max_detected_objects\" lines [[y0,x0,y1,x1] ...], dtype=np.int16
    """
    # create initial point cloud as coordinates of white pixels in binary threshold image
    cloud = make_point_cloud(thresh)

    # lines not of type uint, because maximize_line can produce negative x in slope calc
    lines = np.zeros((max_detected_objects * ITERATIONS_PER_OBJECT, 4), dtype=np.int16) 
    for i in range(lines.shape[0]):
        if cloud.shape[0] < 2:
            break
        lines[i] = random_vertical_line(cloud)
        if not np.any(lines[i]):
            # RANSAC does not find any line which satisfies the requirements and was not considered already
            continue

        lines[i] = maximize_line(lines[i], thresh)

        cloud = update_point_cloud(cloud, lines[i])
        
    # remove empty lines from lines
    lines = remove_empty_lines(lines,max_detected_objects)

    return lines


def scale_bounding_boxes(img : np.ndarray, bounding_box : np.ndarray, target_size : int) -> np.ndarray:
    """Scale square bounding boxes to \"target_size\"

    Args:
        img (np.ndarray): color image from camera
        bounding_box (np.ndarray): detected bounding boxes (as [n][y0, x0, y1, x1] )
        target_size (int): target size in px (e.g. 32 leads to 32x32 image )

    Returns:
        np.ndarray: cropped and scaled pictures
    """
    
    detected_objects_count = bounding_box.shape[0]
    image_sections = np.empty((detected_objects_count, target_size, target_size, 3), dtype=img.dtype)
    
    for idx in range(detected_objects_count):
            image_sections[idx, :] = scale_bounding_box(img, bounding_box[idx, :], target_size)
        
    return image_sections

def scale_bounding_box(img : np.ndarray, bounding_box : np.ndarray, target_size : int) -> np.ndarray:
    """Scale square bounding box to \"target_size\"

    Args:
        img (np.ndarray): color image from camera
        bounding_box (np.ndarray): single detected bounding box
        target_size (int): target size in px (e.g. 32 leads to 32x32 image)

    Returns:
        np.ndarray: cropped and scaled picture
    """
    y0, x0, y1, x1 = bounding_box
    height = y0 - y1
    width = x1 - x0
    assert(height == width) # has to be sqaure bounding box
    
    scaling_factor = target_size / width
    if scaling_factor > 1.0:
        interpolation_mode = cv2.INTER_LINEAR # zooming
    else:
        interpolation_mode = cv2.INTER_AREA # shrinking
    
    cropped_img = img[y1:y0, x0:x1, :].copy()

    return cv2.resize(cropped_img,None,fx=scaling_factor,
                        fy=scaling_factor,interpolation=interpolation_mode)