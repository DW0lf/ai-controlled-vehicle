#!/usr/bin/env python3

"""Implements ObjectDetector for Jetson."""
import numpy as np
from enum import Enum
from typing import Any, Tuple
import cv2
import torch
from torchvision import transforms
from torch2trt import torch2trt, TRTModule

from process_wrapper import ProcessWrapper
from image_processing import batch_transforms, nn_model
from image_processing.object_detector_logic import *
from sensors.camera_transformation import _calc_xy0_camera_center, CameraTransformer


class Object(Enum):
    """Enum for detectable objects"""

    LIGHT_GREEN = 0
    LIGHT_RED = 1
    LIGHT_YELLOW = 2
    STOP_SIGN = 3
    TURN_LEFT = 4
    TURN_RIGHT = 5
    NONE = 42


class ObjectDetector(ProcessWrapper):
    """Class for object detection, which is executed in a separate
    process."""

    def __init__(self, shared_image_data_ptr: Any, shared_object_data_ptr: Any, 
                 shared_object_certainty_data_ptr: Any, image_procession_event: Any, stop_event: Any, 
                 emergency_stop_event: Any, image_procession_barrier: Any, startup_barrier: Any, 
                 camera_tranformer: CameraTransformer, img_section_size: int, max_object_count: int, 
                 min_certainty: float):
        """Initializes ObjectDetector.

        Args:
            shared_image_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image
            shared_object_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            object data
            shared_object_certainty_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory 
            pointer for object distances
            image_procession_event (multiprocessing.Event): Event for image procession initiation
            emergency_stop_event (multiprocessing.Event): Event for emergency stop signal
            image_procession_barrier (multiprocessing.Barrier): Barrier that synchronizes lane detection and object 
            detection
            startup_barrier (multiprocessing.Barrier): Barrier that synchronizes startup of all processes
            camera_transformer (CameraTransformer): camera transformer instance holding the transformation matrices
            img_section_size (int): Number of pixels of an image section that depicts a recognized object
            max_object_count (int): Maximum number of objects that can be detected at a time
            max_certainty (float): Minimum certainty of detected objects
        """

        self.camera_transformer = camera_tranformer
        self.img_section_size = img_section_size
        self.max_object_count = max_object_count
        self.min_certainty = min_certainty
        self.iterations_per_object = 2

        # init events and barriers
        self.image_procession_event = image_procession_event
        self.stop_event = stop_event
        self.emergency_stop_event = emergency_stop_event
        self.image_procession_barrier = image_procession_barrier
        self.startup_barrier = startup_barrier
        
        # init process
        shared_memory_pointers = (shared_image_data_ptr, shared_object_data_ptr, shared_object_certainty_data_ptr)
        super().__init__(shared_memory_pointers, emergency_stop_event)
        
    def _detect(self, shared_image_data: Any) -> Tuple[np.ndarray, np.ndarray]:
        """Detects objects and computes bounding boxes.

        Args:
            shared_image_data (np.ndarray): OpenCV image

        Returns:
            np.ndarray[5, n][np.uint16]: Detected object data as [[obj_id, y0, x0, y1, x1]*max_obj_count] array
            np.ndarray[n][np.float32]: Object distances as [dist]*max_obj_count array
        """

        detected_objects = np.array([Object.NONE.value, 0, 0, 0, 0], dtype=np.int16)
        detected_objects = np.repeat(detected_objects[np.newaxis, :], self.max_object_count, axis=0)

        img = shared_image_data[IMG_HEIGHT_CROPPED:,:] # crop image
        
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        # apply sobel to detect vertical lines and binarize image
        v_lines = cv2.Sobel(gray,cv2.CV_8UC1,1,0,ksize=3, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT)
        _, thresh = cv2.threshold(v_lines, 30, 255, cv2.THRESH_BINARY) # adaptive threshold
        thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, (3,3))
        thresh = cv2.erode(thresh,self.verticalStructure) # remove random glitter
        # find at most max_object_count lines
        lines = find_vertical_lines(thresh, self.max_object_count)

        # save bounding boxes into detected_objects, no classification
        calculate_bounding_boxes(lines, detected_objects, gray.shape[1])
        
        object_distances = np.zeros((lines.shape[0],), dtype=np.float32)
        for idx, line in enumerate(lines):
            image_point = ObjectDetector._yx_to_xy(line[:2])
            object_point = _calc_xy0_camera_center(image_point, self.camera_transformer.inv_intrinsic_cam_matrix_33, 
                                                   self.camera_transformer.inv_extrinsic_cam_matrix_44)
            object_distances[idx] = object_point[1]
            
        self._classify(shared_image_data, detected_objects)
        detected_objects = detected_objects.astype(np.uint16)
        
        return detected_objects, object_distances
    
    
    
    def _classify(self, shared_image_data: np.ndarray, detected_objects: np.ndarray) -> None:
        """
        Classifies image sections that are framed by detected bounding boxes. Works in-place.

        Args:
            shared_image_data (np.ndarray): OpenCV image
            detected_objects (np.ndarray): Detected object data as [[obj_id, y0, x0, y1, x1]*max_obj_count] array
        """
        
        detected_objects_mask = np.any(detected_objects[:, 1:], axis=1)
        actually_detected_objects = detected_objects[detected_objects_mask]
        
        if actually_detected_objects.shape[0] == 0:
            return
        
        image_sections = scale_bounding_boxes(shared_image_data, actually_detected_objects[:, 1:], self.img_section_size)
        image_sections = np.moveaxis(image_sections, -1, 1)
        
        nn_input = self.transform(torch.from_numpy(image_sections)).cuda()
        nn_output = torch.softmax(self.model_trt(nn_input), dim=1)
        #print("NN OUTPUT", nn_output.shape)
        detected_obj_certainty, detected_obj_ids = torch.max(nn_output, dim=1)
        #print("CERTAINTY", detected_obj_certainty)
        #print("IDS", detected_obj_ids.shape)
        
        detected_obj_idx = 0
        for idx in range(detected_objects.shape[0]):
            if not np.any(detected_objects[idx, 1:5]):
                continue
            
            detected_obj_id = detected_obj_ids[detected_obj_idx]
            #print("TEST", detected_obj_certainty[detected_obj_idx])
            if detected_obj_certainty[detected_obj_idx] >= self.min_certainty:
                detected_objects[idx, 0] = detected_obj_id
            detected_obj_idx += 1
        

    def _init(self):
        """Initializes ObjectDetector process specifically."""
        
        # init vertical filter kernel 
        self.verticalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 7))
        
        # init data transform
        self.normalization = batch_transforms.Normalize((0.32175523, 0.31878638, 0.26313573), (0.11819864, 0.122116566, 0.13291316))
        self.transform = transforms.Compose([batch_transforms.ToTensor(), self.normalization])
        
        # load neural network
        nn_model_path = "../nn/wrn_10_4_trt.pth"
        self.model_trt = TRTModule()
        self.model_trt.load_state_dict(torch.load(nn_model_path))
        self.model_trt = self.model_trt.eval().cuda()
        

    def _run(self, shared_image_data_ptr: Any, shared_object_data_ptr: Any, shared_object_certainty_data_ptr: Any):
        """
        Cyclic worker method, detects objects, estimates their distances and stores data in shared memory.

        Args:
            shared_image_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image
            shared_object_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            object data
            shared_object_certainty_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory 
            pointer for object distances
        """
        
        detected_objects = None
        object_distances = None

        # create numpy arrays, representing views onto shared memory
        shared_img_data = np.frombuffer(shared_image_data_ptr, dtype=np.uint8).reshape(int(self.camera_transformer.roi[3]), 
                                                                                       int(self.camera_transformer.roi[2]), 3)
        shared_object_data = np.frombuffer(shared_object_data_ptr, dtype=np.int16).reshape((self.max_object_count, 5))
        shared_distance_data = np.frombuffer(shared_object_certainty_data_ptr, dtype=np.float32)

        # wait until all processes are ready on startup
        self.logger.debug("Waiting for Startup...")
        self.startup_barrier.wait()

        while True:
            # wait until new image has been captured
            self.logger.debug("Waiting for E_2...")
            self.image_procession_event.wait()
            self.logger.debug("E_2 set. Detecting objects...")
            
            # wait, if stop signal was given by remote
            if not self.stop_event.is_set():
                self.logger.info("Stopped until remote gives signal to proceed.")
            self.stop_event.wait() 

            # detect objects, estimate their distances and store data in shared memory
            detected_objects, object_distances = self._detect(shared_img_data)
            np.copyto(shared_object_data, detected_objects)
            np.copyto(shared_distance_data, object_distances)

            # wait until lane detection has been finished
            self.logger.debug("Objects detected. Waiting for Barrier...")
            self.image_procession_barrier.wait()
            self.logger.debug("Barrier passed.")

    def _on_termination(self):
        """Is invoked on process termination."""
        
        del self.model_trt
    
    @staticmethod
    def _yx_to_xy(point: np.ndarray) -> np.ndarray:
        """Flips the order of coordinates from y, x to x, y.

        Args:
            point (np.ndarray): point coordinates in order y, x

        Returns:
            np.ndarray: point coordinates in order x, y
        """
        return np.flip(point)
    
        