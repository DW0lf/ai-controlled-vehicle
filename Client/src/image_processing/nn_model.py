import torch
from torch.tensor import Tensor
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.modules.module import Module


class ResidualBlock(nn.Module):
    def __init__(self, in_channels: int, out_channels: int, stride: int, dropout_rate: float) -> None:
        """
        Represents a single wide residual block, incorporating ReLU, batch normalization and dropout.

        Args:
            in_channels (int): Number of incoming channels
            out_channels (int): Number of outcoming channels
            stride (int): Stride for convolution
            dropout_rate (float): Percentage that a layer's neurons are dropped during training
        """
        
        super(ResidualBlock, self).__init__()
        self.bn1 = nn.BatchNorm2d(in_channels)
        self.relu1 = nn.ReLU(inplace=True)
        self.conv1 = nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=stride, padding=1, bias=False)
        self.dropout = nn.Dropout2d(p=dropout_rate)
        self.bn2 = nn.BatchNorm2d(out_channels)
        self.relu2 = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(out_channels, out_channels, kernel_size=3, stride=1, padding=1, bias=False)
        
        self.equal_in_out = (in_channels == out_channels)
        if not self.equal_in_out:
            self.conv_shortcut = nn.Conv2d(in_channels, out_channels, kernel_size=1, stride=stride, padding=0, bias=False)
        else:
            self.conv_shortcut = None
        
    def forward(self, x: Tensor) -> Tensor:
        """
        Performs forward pass of a single wide residual block.

        Args:
            x (Tensor): Input data

        Returns:
            Tensor: Output data
        """
        
        if self.equal_in_out:
            out = self.bn1(x)
            out = self.relu1(out)
            
        else:
            x = self.bn1(x)
            x = self.relu1(x)
            x, out = self.conv_shortcut(x), x
        
        out = self.conv1(out)
        out = self.dropout(out)
        out = self.bn2(out)
        out = self.relu2(out)
        out = self.conv2(out)

        return torch.add(x, out)
    

class AvgPoolBlock(nn.Module):
    def __init__(self, channels: int) -> None:
        """
        Represents an average pooling block, incorporating ReLU and batch normalization.

        Args:
            channels (int): Number of input/output channels
        """
        
        super(AvgPoolBlock, self).__init__()
        self.bn1 = nn.BatchNorm2d(channels)
        self.relu1 = nn.ReLU(inplace=True)
        self.avg_pool = nn.AvgPool2d(8)
        
    def forward(self, x: Tensor) -> Tensor:
        """
        Performs forward pass of an average pooling block.

        Args:
            x (Tensor): Input data

        Returns:
            Tensor: Output data
        """
        
        x = self.bn1(x)
        x = self.relu1(x)
        return self.avg_pool(x)
    

class TopLevelBlock(nn.Module):
    def __init__(self, N: int, in_channels: int, out_channels: int, stride: int, dropout_rate: float) -> None:
        """
        Represents a cohesive top-level convolutional block in the WideResNet architecture.

        Args:
            N (int): Number of residual blocks
            in_channels (int): Number of input channels
            out_channels (int): Number of output channels
            stride (int): Input stride for first convolution
            dropout_rate (float): Percentage that a layer's neurons are dropped during training
        """
        
        super(TopLevelBlock, self).__init__()
        self.layer = self._create_layer(N, in_channels, out_channels, stride, dropout_rate)
        
    def _create_layer(self, N: int, in_channels: int, out_channels: int, stride: int, dropout_rate: float) -> Module:
        """
        Creates a cohesive top-level convolutional block in the WideResNet architecture.

        Args:
            N (int): Number of residual blocks
            in_channels (int): Number of input channels
            out_channels (int): Number of output channels
            stride (int): Input stride for first convolution
            dropout_rate (float): Percentage that a layer's neurons are dropped during training

        Returns:
            Module: Cohesive top-level convolutional block in the WideResNet architecture
        """
        
        layers = []
        
        for i in range(N):
            if i == 0:
                res_block = ResidualBlock(in_channels, out_channels, stride, dropout_rate)
            else:
                res_block = ResidualBlock(out_channels, out_channels, 1, dropout_rate)
            layers.append(res_block)
            
        return nn.Sequential(*layers)
    
    def forward(self, x: Tensor) -> Tensor:
        """
        Performs forward pass of a cohesive top-level convolutional block in the WideResNet architecture.

        Args:
            x (Tensor): Input data

        Returns:
            Tensor: Output data
        """
        
        return self.layer(x)


class WideResNet(nn.Module):
    def __init__(self, num_classes: int, d: int = 28, k: int = 1, dropout_rate: float = 0) -> None:
        """
        Represents WideResNet.

        Args:
            num_classes (int): Number of classes
            d (int, optional): Number of layers. Defaults to 28.
            k (int, optional): Width multiplier. Defaults to 1.
            dropout_rate (float, optional): Percentage that a layer's neurons are dropped during training. Defaults to 0.
        """
        
        super(WideResNet, self).__init__()
        
        self.channel_num = [16, 16*k, 32*k, 64*k]
        
        assert((d - 4) % 6 == 0)
        N = (d - 4) // 6
        
        # 1st conv before any cohesive top-level block
        self.conv1 = nn.Conv2d(3, self.channel_num[0], kernel_size=3, stride=1, padding=1, bias=False)
        # 1st cohesive top-level block
        self.block1 = TopLevelBlock(N, self.channel_num[0], self.channel_num[1], 1, dropout_rate)
        # 2nd cohesive top-level block
        self.block2 = TopLevelBlock(N, self.channel_num[1], self.channel_num[2], 2, dropout_rate)
        # 3rd cohesive top-level block
        self.block3 = TopLevelBlock(N, self.channel_num[2], self.channel_num[3], 2, dropout_rate)
        
        # average pooling
        self.avg_pool_block = AvgPoolBlock(self.channel_num[3])
        
        # fully connected layer as classifier
        self.fc = nn.Linear(self.channel_num[3], num_classes)

        # weights initialization
        self.init_weights()
    
    def init_weights(self) -> None:
        """Performs weights initialization"""
        
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            elif isinstance(m, nn.Linear):
                m.bias.data.zero_()

    def forward(self, x):
        """
        Performs forward pass of WideResNet.

        Args:
            x (Tensor): Input data

        Returns:
            Tensor: Output data
        """
        
        x = self.conv1(x)
        x = self.block1(x)
        x = self.block2(x)
        x = self.block3(x)
        x = self.avg_pool_block(x)
        x = x.view(-1, self.channel_num[-1])
        return self.fc(x)