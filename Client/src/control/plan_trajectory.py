from typing import Tuple
import numpy as np
import numba

from sensors.camera_transformation import _calc_xy0_camera_center
from image_processing.join_lines import _calc_line_coefficients, MIN_VERTICAL_SLOPE


MIN_HORIZONTAL_LENGTH_PX = 100
MIN_VERTICAL_TRAJECTORY_SLOPE = 1 / MIN_VERTICAL_SLOPE
PSEUDO_METERS_TO_PX = 150
MAX_TRAJECTORY_LENGTH_PX = 150 # is also specified in Server/src/network/izmq_receiver.py
TRAJECTORY_Y_OFFSET_PX = 20
LANE_Y_MASK = np.array([0, 1, 0, 1], dtype=np.uint8)
# minimal distance of horizontal lane from car to stop
DISTANCE_TO_STOP = 0.25
# maximal distance of lines from car to take them into consideration for vertical lane based trajectory
DISTANCE_TO_STEER = 0.35
DISTANCE_WEIGTH = 0.3
LENGTH_WEIGTH = 0.7
assert DISTANCE_WEIGTH + LENGTH_WEIGTH == 1

@numba.njit(cache=True)
def get_lane_vec(lane: np.ndarray) -> np.ndarray:
    """
    Returns 2d vector in the camera frame that describes the lane. The returned vector is oriented from "bottom to top".

    Args:
        lane (np.ndarray): Lane input

    Returns:
        np.ndarray: Lane vector
    """
    return lane[0:2] - lane[2:4]

@numba.njit(cache=True)
def clip_lane_based_trajectory(trajectory: np.ndarray) -> None:
    """
    Limits length of the trajectory path computed from lanes. Clipping is performed in-place.

    Args:
        trajectory (np.ndarray): Unclipped trajectory path
    """
    
    trajectory_vec = get_lane_vec(trajectory)
    trajectory_length = np.linalg.norm(trajectory_vec)
    
    if trajectory_length > MAX_TRAJECTORY_LENGTH_PX:
        trajectory_vec *= (MAX_TRAJECTORY_LENGTH_PX / trajectory_length)
        trajectory[0:2] = trajectory[2:4] + trajectory_vec
    
@numba.njit(cache=True)
def analyse_lanes(lanes: np.ndarray) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """
    Computes length of lanes, and determines vertical lanes as well as whether lanes occur left-sided, right-sided, 
    both-sided or only horizontally.

    Args:
        lanes (np.ndarray): Detected lanes as 2D np.float32 array

    Returns:
        Tuple[np.ndarray, np.ndarray, np.ndarray]: Lane lengths and flag array that indicates vertical lanes
    """
    
    lane_count = lanes.shape[0]
    lane_lengths = np.empty((lane_count, ), dtype=np.float32)
    
    vertical_lane_flags = np.zeros((lane_count,), dtype=np.uint8)
    for idx in range(lane_count):
        lane_lengths[idx] = np.linalg.norm(get_lane_vec(lanes[idx]))
        slope = _calc_line_coefficients(lanes[idx])[0]
        if abs(slope) >= MIN_VERTICAL_SLOPE:
            vertical_lane_flags[idx] = 1
    
    return lane_lengths, vertical_lane_flags

@numba.njit(cache=True)
def compute_vertical_lane_based_trajectory(lanes: np.ndarray, lane_lengths: np.ndarray, 
                                           vertical_lane_flags: np.ndarray, inv_intrinsic_mtx_33: np.ndarray, 
                                           inv_extrinsic_matrix_44: np.ndarray,) -> np.ndarray:
    """
    Computes trajectory that is based on vertical lanes.

    Args:
        lanes (np.ndarray): Detected lanes
        lane_lengths (np.ndarray): Lane lengths
        vertical_lane_flags (np.ndarray): Flag array that indicates vertical lanes

    Returns:
        np.ndarray: Trajectory based on vertical lanes
    """
    
    lane_count = lanes.shape[0]
    
    # weight lanes by length
    vertical_lane_lengths_sum = lane_lengths[vertical_lane_flags > 0].sum()
    lane_length_divisors = np.ones((lane_count,), dtype=np.float32)
    lane_length_divisors[vertical_lane_flags > 0] = vertical_lane_lengths_sum
    lane_weights = lane_lengths / lane_length_divisors
    
    # weight lanes by distance from car
    distances = np.inf * np.ones((lane_count,), dtype=np.float32)
    for idx, lane in enumerate(lanes):
        if vertical_lane_flags[idx] == 0:
            # skip horizontal lines
            continue
        
        if not np.any(lane):
            # skip empty lines
            continue
        
        # compute distance for bottom point of line
        point_f_world = _calc_xy0_camera_center(lanes[idx, 2:], inv_intrinsic_mtx_33, inv_extrinsic_matrix_44)
        distance = point_f_world[1]
        if distance < DISTANCE_TO_STEER:
            distances[idx] = distance
            
    distances = 1 / distances
    
    if np.any(distances):
        distance_sum = distances.sum()
        distances /= distance_sum

    lane_weights[vertical_lane_flags] *= LENGTH_WEIGTH
    lane_weights[vertical_lane_flags] += DISTANCE_WEIGTH * distances[vertical_lane_flags]
    
    trajectory = (lanes[vertical_lane_flags > 0].T @ lane_weights[vertical_lane_flags > 0])

    return trajectory

@numba.njit(cache=True)
def compute_horizontal_lane_based_trajectory(lanes: np.ndarray, lane_lengths: np.ndarray, inv_intrinsic_mtx_33: np.ndarray, inv_extrinsic_matrix_44: np.ndarray, roi_height: int, image_width: int,
                                             vertical_lane_flags: np.ndarray) -> np.ndarray:
    """
    Computes trajectory that is based on horizontal lanes.

    Args:
        lanes (np.ndarray): Detected lanes
        lane_lengths (np.ndarray): Lane lengths
        inv_intrinsic_mtx_33(nd.array): inverted intrinsic camera matrix of shape 3 x 3
        inv_extrinsic_matrix_44(nd.array): inverted extrinsic camera matrix of shape 4 x 4
        roi_height(int): height of roi in pixel
        image_width(int): initial width of image
        vertical_lane_flags (np.ndarray): Flag array that indicates vertical lanes

    Returns:
        np.ndarray: Trajectory based on horizontal lanes
    """
    
    lane_count = lanes.shape[0]
    horizontal_lane_distances = np.inf * np.ones((lane_count, ), dtype=np.float32)
    for idx in range(lane_count):
        if vertical_lane_flags[idx]:
            continue
        lane = lanes[idx]
        
        if not np.any(lane):
            # skip empty lines
            continue
        
        y_center = np.mean(lane[LANE_Y_MASK > 0])
        center_point = np.array([image_width, y_center + roi_height], dtype=np.float32)
        # transform from camera frame to world frame
        center_point_wf = _calc_xy0_camera_center(center_point, inv_intrinsic_mtx_33, inv_extrinsic_matrix_44)
        distance = center_point_wf[1]
        horizontal_lane_distances[idx] = distance if lane_lengths[idx] > MIN_HORIZONTAL_LENGTH_PX and distance < DISTANCE_TO_STOP else np.inf
    
    closest_horizontal_lane_distance = np.min(horizontal_lane_distances)
    if closest_horizontal_lane_distance == np.inf:
        return np.array([0, 0, 0, 0], dtype=np.float32)
    trajectory_length = int(PSEUDO_METERS_TO_PX * closest_horizontal_lane_distance)
    return np.array([0, 0, 0, trajectory_length], np.float32)

@numba.njit(cache=True)
def fuse_lane_based_trajectories(vertical_lane_based_trajectory: np.ndarray, 
                                 horizontal_lane_based_trajectory: np.ndarray) -> np.ndarray:
    """
    Fuses vertical and horizontal lane based trajectories.

    Args:
        vertical_lane_based_trajectory (np.ndarray): Trajectory based on vertical lanes
        horizontal_lane_based_trajectory (np.ndarray): Trajectory based on horizontal lanes

    Returns:
        np.ndarray: Fused trajectory based on lanes
    """
    
    vertical_lane_based_trajectory_slope = _calc_line_coefficients(vertical_lane_based_trajectory)[0]
    
    # if vertical lane based trajectory is approximately vertical, only consider horizontal lane based trajectory
    if abs(vertical_lane_based_trajectory_slope) >= MIN_VERTICAL_TRAJECTORY_SLOPE:
        if np.linalg.norm(get_lane_vec(horizontal_lane_based_trajectory)) > 0:
            return horizontal_lane_based_trajectory
        else:
            return vertical_lane_based_trajectory
    # otherwise, horizontal lane based trajectory is misleading 
    else:
        if np.linalg.norm(get_lane_vec(vertical_lane_based_trajectory)) > 0:
            return vertical_lane_based_trajectory
        else:
            return horizontal_lane_based_trajectory

@numba.njit(cache=True)
def compute_lane_based_trajectory_vector(lanes: np.ndarray, inv_intrinsic_mtx_33: np.ndarray, inv_extrinsic_matrix_44: np.ndarray, img_width: int, roi_height: int) -> np.ndarray:
    """
    Interface for computation of lane based trajectory..

    Args:
        lanes (np.ndarray): Detected lanes
        inv_intrinsic_mtx_33(nd.array): inverted intrinsic camera matrix of shape 3 x 3
        inv_extrinsic_matrix_44(nd.array): inverted extrinsic camera matrix of shape 4 x 4
        img_width(int): initial image width
        roi_height(int): height of roi in pixels

    Returns:
        np.ndarray: Trajectory vector based on vertical and horizontal
    """
    lanes_float = lanes.astype(np.float32)
    lane_lengths, vertical_lane_flags = analyse_lanes(lanes_float)
    
    if np.any(vertical_lane_flags):
        vertical_lane_based_trajectory = compute_vertical_lane_based_trajectory(lanes_float, lane_lengths, 
                                                                                vertical_lane_flags, inv_intrinsic_mtx_33, inv_extrinsic_matrix_44)
    else:
        vertical_lane_based_trajectory = np.zeros((4,), dtype=np.float32)   
    clip_lane_based_trajectory(vertical_lane_based_trajectory)
    
    if np.any(vertical_lane_flags == 0):
        horizontal_lane_based_trajectory = compute_horizontal_lane_based_trajectory(
            lanes_float, lane_lengths, inv_intrinsic_mtx_33, inv_extrinsic_matrix_44, roi_height, img_width, vertical_lane_flags)
    else:
        horizontal_lane_based_trajectory = np.zeros((4,), dtype=np.float32)
    clip_lane_based_trajectory(horizontal_lane_based_trajectory)
    
    lane_based_trajectory = fuse_lane_based_trajectories(vertical_lane_based_trajectory, 
                                                         horizontal_lane_based_trajectory)

    return lane_based_trajectory.astype(np.uint16)

@numba.njit(cache=True)
def line_to_trajectory_components(line_vector: np.ndarray) -> np.ndarray:
    """Compute the length and the slope of the direction vector.

    Args:
        trajectory_vector (np.ndarray): direction vector for trajectory (direction indicating the steering
                                        angle, length indicating the speed)

    Returns:
        np.ndarray: array containing length and slope of direction vector
    """
    dy = line_vector[3] - line_vector[1]
    dx = line_vector[2] - line_vector[0]
    
    # invert dx because points are sorted by y values -> correct slope
    dx *= -1
    
    if dx == 0:
        slope = np.inf
    else:
        slope = dy / dx
    
    length = np.sqrt((dy ** 2 + dx ** 2))
    
    return np.array([slope, length], dtype=np.float32)

@numba.njit(cache=True)
def compute_lwma(arr: np.ndarray, weight_matrix: np.ndarray) -> np.ndarray:
    """Computes linear weighted moving average (LWMA).

    Args:
        arr (np.ndarray): input array (n x m)
        weight_matrix (np.ndarray): weight matrix (u x u: u is maximal number of elements)

    Returns:
        np.ndarray: linear weighted moving averaged elements
    """
    element_count = arr.shape[0]
    return arr.astype(np.float32).T @ weight_matrix[element_count-1, :element_count]

@numba.njit(cache=True)
def create_weight_mat(length: int) -> np.ndarray:
    """Generates matrix of weights for linear weighted moving average computation.

    Args:
        length (int): maximal number of elements

    Returns:
        np.ndarray: weights as length x length matrix
    """
    weights_mat = np.zeros((length, length), dtype=np.float32)
    for idx_outer in range(length):
        n = idx_outer + 1
        for idx_inner in range(n):
            weights_mat[idx_outer, idx_inner] = 2 * (idx_inner + 1) / (n * (n + 1))
    return weights_mat

@numba.njit(cache=True)
def proximity_alert(ultrasonic_data : np.ndarray, current_speed : int, neutral_speed : int) -> bool:
    """Checks all mounted ultrasonic sensors against the maximum allowed proximity between the car and an obstacle

    Args:
        ultrasonic_data (np.ndarray[3][np.float32]): Ultrasonic measurements as [REAR, FRONT, RIGHT] array
        current_speed (int): the current speed angle
        neutral_speed (int): neutral speed angle

    Returns:
        bool: Returns True if vehicle should stop, else False
    """
    # car is driving forwards --> check front sensor
    # [REAR, FRONT, RIGHT]
    if current_speed > neutral_speed:  
        if ultrasonic_data[1] < DISTANCE_TO_STOP:
            return True
    # car is driving backwards (since REAR is deactivated no check necessary)
    # elif current_speed < neutral_speed:
    #     if ultrasonic_data[0] < DISTANCE_TO_STOP:
    #         return True
    # TODO: deactivated REAR and RIGHT sensor
    return False
