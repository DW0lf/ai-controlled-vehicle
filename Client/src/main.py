#!/usr/bin/env python3

"""Implements main script to be started on the jetson."""

import argparse
import logging
from multiprocessing import Event, Barrier, Value, Array
from multiprocessing.sharedctypes import RawArray
import os
import sys

import yaml

from control.car_controller import CarController
from copy_worker import CopyWorker
from image_processing.lane_detector import LaneDetector
from image_processing.object_detector import ObjectDetector
from logging_ import logging_factory, logging_redirection
from network.receiver import Receiver
from network.transmitter import Transmitter
from process_wrapper import ProcessWrapper
from sensors.camera import Camera
from sensors.ultrasonic_sensor import UltrasonicSensorsManager
from sensors.camera_transformation import CameraTransformer

def main():
    """Main function of the script"""
    
    # --- INITIALIZATION ---
    # parse command line arguments
    parser = argparse.ArgumentParser(description="AI controlled vehicle")
    parser.add_argument("-d", "--debug", default=False, action='store_true', help="activates debug mode, including more verbose logging")
    args = parser.parse_args()
    DEBUG_MODE = args.debug
    ProcessWrapper.DEBUG_MODE = DEBUG_MODE
    
    # defining emergency stop event
    emergency_stop_event = Event()
    
    # setup logging
    logging_name = "Main"
    if DEBUG_MODE:
        logger = logging_factory.create_logger(logging_name, logging.DEBUG)
    else:
        logger = logging_factory.create_logger(logging_name)
    sys.excepthook = logging_redirection.redirect_exception(logger)
    
    logger.info("AI controlled vehicle has been started.")
    
    # read configuration
    config_file_name = os.path.join(os.path.dirname(__file__), "../config", "client_config.yml")
    logger.info("Reading configuration file...")
    with open(config_file_name, 'r') as file:
        config = yaml.safe_load(file)
    # get parameters from read-in configuration
    CAM_CAPTURE_WIDTH, CAM_CAPTURE_HEIGHT = int(config["CAM"]["CAPTURE_WIDTH"]), int(config["CAM"]["CAPTURE_HEIGHT"])
    CAM_OUTPUT_WIDTH, CAM_OUTPUT_HEIGHT = int(config["CAM"]["OUTPUT_WIDTH"]), int(config["CAM"]["OUTPUT_HEIGHT"])
    CAM_FPS = config["CAM"]["CAPTURE_FPS"]
    MAX_DETECTABLE_LANES = int(config["LANE_DETECTION"]["MAX_DETECTABLE_LANES"])
    IMG_SECTION_SIZE = int(config["OBJ_DETECTION"]["IMG_SECTION_SIZE"])
    MAX_DETECTABLE_OBJ = int(config["OBJ_DETECTION"]["MAX_DETECTABLE_OBJ"])
    MIN_CERTAINTY = float(config["OBJ_DETECTION"]["MIN_CERTAINTY"])
    ULTRASONIC_HISTORY_LENGTH = int(config["ULTRASONIC_SENSOR"]["HISTORY_LENGTH"])
    ULTRASONIC_SENSOR_COUNT = len([1 for sensor_config in config["ULTRASONIC_SENSOR"]["SENSORS"].values() 
                                     if int(sensor_config["ACTIVE"])])
    NEUTRAL_SPEED = int(config["CAR_CONTROLLER"]["ENGINE"]["NEUTRAL_SPEED"])
    NEUTRAL_STEERING = int(config["CAR_CONTROLLER"]["STEER"]["NEUTRAL_STEERING"])
    IMG_SHAPE = (CAM_OUTPUT_HEIGHT, CAM_OUTPUT_WIDTH, 3)
    logger.info("Configuration file has been read.")
    
    # initialize CameraTransformer, which loads camera transformation parameters from files
    camera_transformer = CameraTransformer(CAM_OUTPUT_WIDTH, CAM_OUTPUT_HEIGHT)
    CAM_ROI_X, CAM_ROI_Y = int(camera_transformer.roi[0]), int(camera_transformer.roi[1])
    CAM_ROI_W, CAM_ROI_H = int(camera_transformer.roi[2]), int(camera_transformer.roi[3])
    IMG_ROI = (CAM_ROI_X, CAM_ROI_Y, CAM_ROI_W, CAM_ROI_H)

    # allocate shared memory
    logger.info("Allocating shared memory...")
    shared_image_data_ptr = RawArray('B', CAM_ROI_W * CAM_ROI_H * 3)
    shared_image_data_cpy_ptr = RawArray('B', CAM_ROI_W * CAM_ROI_H * 3)
    shared_lane_data_ptr = RawArray('H', MAX_DETECTABLE_LANES * 4)
    shared_lane_data_cpy_ptr = RawArray('H', MAX_DETECTABLE_LANES * 4)
    shared_object_data_ptr = RawArray('H', MAX_DETECTABLE_OBJ * 5)
    shared_object_data_cpy_ptr = RawArray('H', MAX_DETECTABLE_OBJ * 5)
    shared_object_certainty_data_ptr = RawArray('f', MAX_DETECTABLE_OBJ)
    shared_object_certainty_data_cpy_ptr = RawArray('f', MAX_DETECTABLE_OBJ)
    shared_ultrasonic_data_ptr = Array('f', ULTRASONIC_SENSOR_COUNT)
    shared_ultrasonic_data_cpy_ptr = RawArray('f', ULTRASONIC_SENSOR_COUNT)
    shared_remote_ip_address_data_ptr = RawArray('B', 16)
    shared_control_speed_data_ptr = Value('B', NEUTRAL_SPEED)
    shared_control_steering_data_ptr = Value('B', NEUTRAL_STEERING)
    shared_manual_control_data_ptr = Value('B', 0)
    shared_working_frequency_data_ptr = Value('f', -1.)
    logger.info("Shared memory has been allocated.")
    
    # define process sync mechanisms (events and barriers)
    logger.info("Defining process sync mechanics...")
    connection_event = Event()
    startup_barrier = Barrier(9)
    image_capture_event = Event()
    image_procession_event = Event()
    image_procession_barrier = Barrier(2)
    data_sending_event = Event()
    data_copy_event = Event()
    manual_control_event = Event()
    control_application_event = Event()
    stop_event = Event()
    # unset stop event leads to pausing behavior on startup
    logger.info("Process sync mechanics have been defined.")

    # --- SETUP OTHER PROCESSES ---
    # start car controller process
    logger.info("Initializing Car Controller process...")
    car_controller = CarController(shared_control_speed_data_ptr, shared_control_steering_data_ptr, 
                                   shared_manual_control_data_ptr, shared_lane_data_ptr, shared_object_data_ptr, 
                                   shared_object_certainty_data_ptr, shared_ultrasonic_data_ptr,
                                   control_application_event, data_copy_event, data_sending_event, image_capture_event,
                                   stop_event, emergency_stop_event, startup_barrier, config["CAR_CONTROLLER"], 
                                   camera_transformer,
                                   MAX_DETECTABLE_LANES, MAX_DETECTABLE_OBJ, NEUTRAL_SPEED, NEUTRAL_STEERING)
    car_controller.start()
    logger.info("Car Controller process has been started.")

    # start tcp receiver process
    logger.info("Initializing TCP Receiver process...")
    receiver = Receiver(shared_control_speed_data_ptr, shared_control_steering_data_ptr, shared_manual_control_data_ptr,
                        shared_remote_ip_address_data_ptr, control_application_event, connection_event, 
                        manual_control_event, stop_event, emergency_stop_event, startup_barrier, 
                        config["CONNECTION"]["TCP_PORT"])
    receiver.start()
    logger.info("TCP Receiver process has been started.")

    # start ultrasonic manager process
    logger.info("Initializing Ultrasonic Sensor Manager process...")
    ultrasonic_manager = UltrasonicSensorsManager(shared_ultrasonic_data_ptr, stop_event, emergency_stop_event, 
                                                  startup_barrier, config["ULTRASONIC_SENSOR"]["SENSORS"],
                                                  ULTRASONIC_HISTORY_LENGTH)
    ultrasonic_manager.start()
    logger.info("Ultrasonic Sensor Manager process has been started.")
    
    # start camera process
    logger.info("Initializing Camera process...")
    camera = Camera(shared_image_data_ptr, shared_working_frequency_data_ptr, shared_manual_control_data_ptr, 
                    image_capture_event, image_procession_event, manual_control_event, stop_event, emergency_stop_event,
                    startup_barrier, CAM_CAPTURE_WIDTH, CAM_CAPTURE_HEIGHT, CAM_FPS, IMG_SHAPE, IMG_ROI)
    camera.start()
    logger.info("Camera process has been started.")

    # start lane detection process
    logger.info("Initializing Lane Detection process...")
    lane_detector = LaneDetector(shared_image_data_ptr, shared_lane_data_ptr, shared_manual_control_data_ptr, 
                                 image_procession_event, data_copy_event, data_sending_event, control_application_event,
                                 image_capture_event, stop_event, emergency_stop_event, image_procession_barrier, 
                                 startup_barrier, IMG_ROI, MAX_DETECTABLE_LANES)
    lane_detector.start()
    logger.info("Lane Detection process has been started.")

    # start object detection process
    logger.info("Initializing Object Detection process...")
    object_detector = ObjectDetector(shared_image_data_ptr, shared_object_data_ptr, shared_object_certainty_data_ptr,
                                     image_procession_event, stop_event, emergency_stop_event, image_procession_barrier, 
                                     startup_barrier, camera_transformer, IMG_SECTION_SIZE, MAX_DETECTABLE_OBJ, 
                                     MIN_CERTAINTY)
    object_detector.start()
    logger.info("Object Detection process has been started.")
    
    # start copy worker process
    logger.info("Initializing Copy Worker process...")
    copy_worker = CopyWorker(shared_image_data_ptr, shared_lane_data_ptr, shared_object_data_ptr, 
                             shared_object_certainty_data_ptr, shared_ultrasonic_data_ptr, shared_image_data_cpy_ptr,
                             shared_lane_data_cpy_ptr, shared_object_data_cpy_ptr, 
                             shared_object_certainty_data_cpy_ptr, shared_ultrasonic_data_cpy_ptr, data_copy_event,
                             data_sending_event, image_capture_event, stop_event, emergency_stop_event, startup_barrier,
                             IMG_ROI, MAX_DETECTABLE_LANES, MAX_DETECTABLE_OBJ)
    copy_worker.start()
    logger.info("Copy Worker process has been started.")
    
    # start izmq transmitter process
    logger.info("Initializing ImageZMQ Transmitter process...")
    transmitter = Transmitter(shared_remote_ip_address_data_ptr, shared_working_frequency_data_ptr, 
                              shared_image_data_cpy_ptr, shared_lane_data_cpy_ptr, shared_object_data_cpy_ptr, 
                              shared_object_certainty_data_cpy_ptr, shared_ultrasonic_data_cpy_ptr, 
                              shared_control_speed_data_ptr, shared_control_steering_data_ptr, data_sending_event, 
                              connection_event, stop_event, emergency_stop_event, startup_barrier, IMG_ROI, 
                              MAX_DETECTABLE_LANES, MAX_DETECTABLE_OBJ, config["ULTRASONIC_SENSOR"]["SENSORS"], 
                              config["CONNECTION"]["IZMQ_PORT"], config["CONNECTION"]["IZMQ_JPG_QUALITY"])
    transmitter.start()
    logger.info("ImageZMQ Transmitter process has been started.")
    
    # wait until all processes are ready on startup
    logger.info("Waiting, until all processes are ready...")
    startup_barrier.wait()
    
    # --- STARTUP ---
    # check for emergency stop after process initialization
    if emergency_stop_event.is_set():
        terminate(car_controller, receiver, ultrasonic_manager, camera, lane_detector, object_detector, copy_worker,
                  transmitter, logger)
        
    logger.info("Regular operations have been started.")
    
    # notify camera to capture first image
    image_capture_event.set()
    logger.debug("Set E_1.")

    # --- TERMINATION ---
    # wait for emergency stop
    emergency_stop_event.wait()
    terminate(car_controller, receiver, ultrasonic_manager, camera, lane_detector, object_detector, copy_worker, 
              transmitter, logger)
    
    
def terminate(car_controller: ProcessWrapper, receiver: ProcessWrapper, ultrasonic_manager: ProcessWrapper, 
              camera: ProcessWrapper, lane_detector: ProcessWrapper, object_detector: ProcessWrapper, 
              copy_worker: CopyWorker, transmitter: ProcessWrapper, logger: logging.Logger):
    """
    Terminates all other processes and finally the main process as well.

    Args:
        car_controller (ProcessWrapper): Car Controller process
        receiver (ProcessWrapper): TCP Receiver process
        ultrasonic_manager (ProcessWrapper): Ultrasonic Sensor Manager process
        camera (ProcessWrapper): Camera process
        lane_detector (ProcessWrapper): Lane Detection process
        object_detector (ProcessWrapper): Object Detection process
        copy_worker (ProcessWrapper): Copy Worker process
        transmitter (ProcessWrapper): Transmitter process
        logger (logging.Logger): Logger of the main process
    """
    
    logger.info("Emergency stop signal has been emitted. All Processes will terminate in a timely manner.")
    
    # terminate car controller process and release its resources
    if car_controller.process.is_alive():
        car_controller.terminate()
    car_controller.process.join()
    logger.info("Car Controller process terminated.")
    
    # terminate TCP receiver process and release its resources
    if receiver.process.is_alive():
        receiver.terminate()
    receiver.process.join()
    logger.info("TCP Receiver process terminated.")
    
    # terminate ultrasonic sensor manager process and release its resources
    if ultrasonic_manager.process.is_alive():
        ultrasonic_manager.terminate()
    ultrasonic_manager.process.join()
    logger.info("Ultrasonic Sensor Manager process terminated.")
    
    # terminate lane detection process and release its resources
    if lane_detector.process.is_alive():
        lane_detector.terminate()
    lane_detector.process.join()
    logger.info("Lane Detection process terminated.")
    
    # terminate object detection process and release its resources
    if object_detector.process.is_alive():
        object_detector.terminate()
    object_detector.process.join()
    logger.info("Object Detection process terminated.")
    
    # terminate copy worker process and release its resources
    if copy_worker.process.is_alive():
        copy_worker.terminate()
    copy_worker.process.join()
    logger.info("Copy Worker process terminated.")
    
    # terminate transmitter process and release its resources
    if transmitter.process.is_alive():
        transmitter.terminate()
    transmitter.process.join()
    logger.info("Transmitter process terminated.")
    
    # terminate camera process and release its resources
    if camera.process.is_alive():
        camera.terminate()
    camera.process.join()
    logger.info("Camera process terminated.")
    
    # terminate main process and release logging resources
    logger.info("AI controlled vehicle has been shutdown.")
    logging.shutdown()
    exit(0)


if __name__ == '__main__':
    main()