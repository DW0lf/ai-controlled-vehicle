#!/usr/bin/env python3

"""Implements camera process for Jetson."""

import time
from typing import Any, Tuple

import cv2
import numpy as np

from process_wrapper import ProcessWrapper


class Camera(ProcessWrapper):
    """Class for image capture, which is executed in a separate process."""

    def __init__(self, shared_image_data_ptr: Any, shared_working_frequency_data_ptr: Any, 
                 shared_manual_control_data_ptr: Any, image_capture_event: Any, image_procession_event: Any, 
                 manual_control_event: Any, stop_event: Any, emergency_stop_event: Any, startup_barrier: Any, 
                 cam_capture_width: int, cam_capture_height: int, cam_capture_fps: str, 
                 img_shape: Tuple[int, int, int], img_roi: Tuple[int, int, int, int]):
        """
        Initializes Camera.

        Args:
            shared_image_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image
            shared_working_frequency_data_ptr (multiprocessing.Value[ctypes.c_float]): Shared memory pointer for working
            frequency
            shared_manual_control_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for manual 
            control flag
            image_capture_event (multiprocessing.Event): Event for image capture initiation
            image_procession_event (multiprocessing.Event): Event for image procession initiation
            manual_control_event (multiprocessing.Event): Event for safe switch from autonomous to manual control
            stop_event (multiprocessing.Event): Event for stop signal
            emergency_stop_event (multiprocessing.Event): Event for emergency stop signal
            startup_barrier (multiprocessing.Barrier): Barrier that synchronizes startup of all processes
            cam_capture_width (int): Original horizontal capture resolution
            cam_capture_height (int): Original vertical capture resolution
            cam_capture_fps (str): Frame rate/frequency [\"<nominator>/<denominator>\"], at which images are captured
            img_shape (Tuple[int, int, int]): Shape of capured images
            img_roi (Tuple[int, int, int, int]): Shape of image's region of interest (x, y, w, h)
        """

        self.cam_capture_width = cam_capture_width
        self.cam_capture_height = cam_capture_height
        self.cam_capture_fps = cam_capture_fps
        self.img_shape = img_shape
        self.img_roi = img_roi

        # init events and barriers
        self.image_capture_event = image_capture_event
        self.image_procession_event = image_procession_event
        self.manual_control_event = manual_control_event
        self.stop_event = stop_event
        self.emergency_stop_event = emergency_stop_event
        self.startup_barrier = startup_barrier
        
        # init process
        shared_memory_pointers = (shared_image_data_ptr, shared_working_frequency_data_ptr,
                                  shared_manual_control_data_ptr)
        super().__init__(shared_memory_pointers, emergency_stop_event)
        
    def _init(self):
        """Initializes Camera process specifically."""
        
        # define gstreamer pipeline
        gstr_pipe = "nvarguscamerasrc wbmode=2 saturation=1.2 exposuretimerange=\"30000000 30000000\" gainrange=\"5 5\"\
                     ispdigitalgainrange=\"1 1\" ! \
                     video/x-raw(memory:NVMM), width=%d, height=%d, format=(string)NV12, framerate=%s ! \
                     nvvidconv flip-method=2 ! \
                     video/x-raw, width=%d, height=%d, format=(string)BGRx ! \
                     videoconvert ! \
                     video/x-raw, format=(string)BGR ! \
                     appsink" % \
                    (self.cam_capture_width, self.cam_capture_height, self.cam_capture_fps, self.img_shape[1], 
                     self.img_shape[0])

        # init cam driver
        self.logger.info("Initializing camera driver...")
        self.camera = cv2.VideoCapture(gstr_pipe, cv2.CAP_GSTREAMER)
        self.logger.info("Camera driver has been initialized.")

        # init all values from camera calibration
        #self.cameraMatrix = np.loadtxt("../cam_calib/params/intrinsic_matrix.csv", delimiter=',')
        #self.newCamMatrix = np.loadtxt("../cam_calib/params/scaled_intrinsic_matrix.csv", delimiter=',') 
        #self.distCoeff = np.loadtxt("../cam_calib/params/camera_distortion.csv", delimiter=',')
        self.mapx = np.loadtxt("../cam_calib/params/mapx.csv", delimiter=',', dtype=np.int16)
        self.mapx = np.reshape(self.mapx, (self.mapx.shape[0] // 2, self.mapx.shape[1], 2))
        self.mapy = np.loadtxt("../cam_calib/params/mapy.csv", delimiter=',', dtype=np.uint16)
        

        # check cam health
        cam_health = self.camera.isOpened()
        if not cam_health:
            self.logger.error("Initialization failed.")
            self.emergency_stop_event.set()

    def _run(self, shared_image_data_ptr: Any, shared_working_frequency_data_ptr: Any, 
             shared_manual_control_data_ptr: Any):
        """
        Cyclic worker method, captures images and stores data in shared memory.

        Args:
            shared_image_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image
            shared_working_frequency_data_ptr (multiprocessing.Value[ctypes.c_float]): Shared memory pointer for working
            frequency
            shared_manual_control_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for manual 
            control flag
        """
        
        capture_success = None
        image = None
        timestamp = time.time()
        old_timestamp = None

        # create numpy arrays, representing views onto shared memory
        shared_img_data = np.frombuffer(shared_image_data_ptr, dtype=np.uint8).reshape(self.img_roi[3], self.img_roi[2], 
                                                                                       3)

        # wait until all processes are ready on startup
        self.logger.debug("Waiting for Startup...")
        self.startup_barrier.wait()

        while True:
            # wait until next image should be captured
            self.logger.debug("Waiting for E_1...")
            self.image_capture_event.wait()
            self.logger.debug("E_1 set. Capturing image...")
            
            # wait, if stop signal was given by remote
            if not self.stop_event.is_set():
                self.logger.info("Stopped until remote gives signal to proceed.")
            self.stop_event.wait()
            
            # measure current working frequency
            old_timestamp = timestamp
            timestamp = time.time()
            shared_working_frequency_data_ptr.value = 1 / (timestamp - old_timestamp)
            
            # switch safely between control modes, if signal was given by remote
            if self.manual_control_event.is_set():
                shared_manual_control_data_ptr.value ^= 1
                self.manual_control_event.clear()

            # capture image
            capture_success, image = self.camera.read()
            if not capture_success:
                self.logger.error("Image capture failed.")
                self.emergency_stop_event.set()
                break

            # undistort image and crop to ROI
            dst = cv2.remap(image, self.mapx, self.mapy, cv2.INTER_LINEAR)
            x,y,w,h = self.img_roi
            dst = dst[y:y+h, x:x+w]

            # store in shared memory
            np.copyto(shared_img_data, dst)

            # reset waiting event
            self.image_capture_event.clear()
            self.logger.debug("Image captured. E_1 cleared.")

            # notify lane and object detection processes
            self.image_procession_event.set()
            self.logger.debug("Set E_2.")

    def _on_termination(self):
        """Is invoked on process termination. Releases camera resources."""
        
        # check if camera is initialized
        if self.camera is not None:
            # release resources
            self.logger.info("Releasing camera resources...")
            self.camera.release()
            del self.camera
            self.logger.info("Camera resources have been released.")
