#!/usr/bin/env python3

"""Implements management of ultrasonic sensors for Jetson."""

from collections import deque
import logging
import time
from typing import Any, Dict

from adafruit_blinka.microcontroller.tegra.t210.pin import Pin
import Jetson.GPIO as GPIO
import numpy as np

from process_wrapper import ProcessWrapper


class UltrasonicSensor:
    """Represents an ultrasonic sensor."""

    def __init__(self, echo_pin: str, trigger_pin: str, name: str, history_length: int, logger: logging.Logger):
        """
        Initializes UltrasonicSensor.

        Args:
            echo_pin (str): Pin ID of echo pin to evaluate
            trigger_pin (str): Pin ID of trigger pin to trigger for signal
            name (str): Name of the sensor
            history_length (int): Length of measurements history, which is used for average computation
            logger (logging.Logger): Logger of the corresponding process
        """

        self.name = name
        self.echo_pin = echo_pin
        self.trigger_pin = trigger_pin
        self.logger = logger
        self.values = deque(maxlen=history_length)

        # initialize I/O pins
        self.logger.info("Initializing I/O pins for ultrasonic sensor \"%s\"..." % self.name)
        GPIO.setup(self.trigger_pin, GPIO.OUT)
        GPIO.output(self.trigger_pin, GPIO.LOW)
        GPIO.setup(self.echo_pin, GPIO.IN)
        self.logger.info("I/O pins for ultrasonic sensor \"%s\" have been initialized." % self.name)

    def measure(self):
        """
        Measures the current distance and append it to the fifo deque. Timings are taken from HC-SR04 datasheet 
        (cf. https://elektro.turanis.de/html/prj121/index.html)
        """


        # create a falling edge for at least 10µs at the trigger
        GPIO.output(self.trigger_pin, GPIO.HIGH)
        time.sleep(0.00001)
        GPIO.output(self.trigger_pin, GPIO.LOW)
        
        # sensor sends burst signal for 200µs, then echo pin is set to high. Nevertheless, wait max 5ms for a rising 
        # edge at echo pin
        GPIO.wait_for_edge(self.echo_pin, GPIO.RISING, timeout=5)
        pulse_start_time = time.time()
        # sensor waits max 200ms for return signal. Nevertheless, wait max 205ms for a falling edge at echo pin
        GPIO.wait_for_edge(self.echo_pin, GPIO.FALLING, timeout=205)
        pulse_end_time = time.time()
        # compute pulse duration
        pulse_duration = pulse_end_time - pulse_start_time
        # since the sensor can only detect obstacles up to 3m distance and sonic has a velocity ~343m/s, max 
        # permissible duration is ~17.5ms (outward and return). However, wait max 25ms due to scheduling delays
        if pulse_duration > 0.025:
            self.logger.debug("Original measured duration of %.3f sec is above permissible threshold (25 ms) for \
                               sensor \"%s\"." % (pulse_duration, self.name))
            pulse_duration = 0
        # compute distance in cm (half sonic velocity ~343m/s)
        distance = round(pulse_duration * 17150, 2)
        self.values.append(distance)

        if not self.values:
            return 0.
        else:
            # to reduce inaccuracies among measurements, compute average value of last x measurements
            return sum(self.values)/len(self.values)


class UltrasonicSensorsManager(ProcessWrapper):
    """Manages requests for all sensors in a separate process."""

    def __init__(self, shared_ultrasonic_data_ptr: Any, stop_event: Any, emergency_stop_event: Any, 
                 startup_barrier: Any, sensor_pins_dict: Dict, sensor_history_length: int):
        """
        Initializes UltrasonicSensorManager.

        Args:
            shared_ultrasonic_data_ptr (multiprocessing.Array[ctypes.c_float]): Shared memory pointer 
            for ultrasonic data
            stop_event (multiprocessing.Event): Event for stop signal
            emergency_stop_event (multiprocessing.Event): Event for emergency stop signal
            startup_barrier (multiprocessing.Barrier): Barrier that synchronizes startup of all processes
            sensor_pins_dict (Dict): Dictionary containing names of sensors as top-level keys, beneath these the 
            keys ECHO and TRIGGER define the sensor specific pins numbers
            sensor_history_length (int): Length of measurements history, which is used for average computation
        """
        
        self.sensor_pins_dict = sensor_pins_dict
        self.sensor_history_length = sensor_history_length

        # init events and barriers
        self.stop_event = stop_event
        self.emergency_stop_event = emergency_stop_event
        self.startup_barrier = startup_barrier
        
        # init process
        shared_memory_pointers = (shared_ultrasonic_data_ptr, )
        super().__init__(shared_memory_pointers, emergency_stop_event)
        
    def _init(self):
        """Initializes UltrasonicSensorManager process specifically."""
        
        self.sensors = list()
        
        # init sensors
        for sensor_name, config in self.sensor_pins_dict.items():
            if int(config["ACTIVE"]):
                self.sensors.append(UltrasonicSensor(config["ECHO"], config["TRIGGER"], sensor_name, 
                                                 self.sensor_history_length, self.logger))
        

    def _run(self, shared_ultrasonic_data_ptr: Any):
        """
        Cyclic worker method, evaluates measurements of ultrasonic sensors and stores data in shared memory.

        Args:
            shared_ultrasonic_data_ptr (multiprocessing.Array[ctypes.c_float]): Shared memory pointer 
            for ultrasonic data
        """

        # create numpy arrays, representing views onto shared memory
        shared_ultrasonic_data = np.frombuffer(shared_ultrasonic_data_ptr.get_obj(), dtype=np.float32)

        # wait until all processes are ready on startup
        self.logger.debug("Waiting for Startup...")
        self.startup_barrier.wait()

        while True:
            self.logger.debug("Starting measurements...")
            
            # wait, if stop signal was given by remote
            if not self.stop_event.is_set():
                self.logger.info("Stopped until remote gives signal to proceed.")
            self.stop_event.wait()
            
            for idx, sensor in enumerate(self.sensors):
                self.logger.debug("Starting measurement for ultrasonic sensor \"%s\"..." % sensor.name)
                shared_ultrasonic_data[idx] = sensor.measure()
                self.logger.debug("Measurement finished for ultrasonic sensor \"%s\". Result: %f" % 
                                  (sensor.name, shared_ultrasonic_data[idx]))
            self.logger.debug("Measurements finished.")
            
            # sleep for 20ms, which is the minimum time between two measurements
            time.sleep(0.02)

    def _on_termination(self):
        """Is invoked on process termination. Releases sensor resources."""
        
        GPIO.cleanup()