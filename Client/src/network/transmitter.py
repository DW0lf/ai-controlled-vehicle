#!/usr/bin/env python3

"""Implements Transmitter (ImageZMQ sender) for Jetson."""

from datetime import datetime
import json
from typing import Any, Dict, Tuple

import cv2
import imagezmq
import numpy as np

from process_wrapper import ProcessWrapper
from image_processing.object_detector import Object

class Transmitter(ProcessWrapper):
    """Class for ImageZMQ Sender, which is executed in a separate process."""

    def __init__(self, shared_remote_ip_address_data_ptr: Any, shared_working_frequency_data_ptr: Any, 
                 shared_image_data_cpy_ptr: Any, shared_lane_data_cpy_ptr: Any, shared_object_data_cpy_ptr: Any, 
                 shared_object_certainty_data_cpy_ptr: Any, shared_ultrasonic_data_cpy_ptr: Any, 
                 shared_control_speed_data_ptr: Any, shared_control_steering_data_ptr: Any, data_sending_event: Any, 
                 connection_event: Any, stop_event: Any, emergency_stop_event: Any, startup_barrier: Any, 
                 img_roi: Tuple[int, int, int, int], max_detectable_lanes: int, max_detectable_obj: int, 
                 ultrasonic_sensors: Dict, izmq_port: int, jpg_quality: int):
        """
        Initializes ImageZMQ Sender.

        Args:
            shared_remote_ip_address_data_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory 
            pointer for remote IP address
            shared_working_frequency_data_ptr (multiprocessing.Value[ctypes.c_float]): Shared memory pointer for working
            frequency
            shared_image_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image copy
            shared_lane_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data copy
            shared_object_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer 
            for object data copy
            shared_object_certainty_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared 
            memory pointer for object distances copy
            shared_ultrasonic_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory 
            pointer for ultrasonic data copy
            shared_control_speed_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for speed value
            shared_control_steering_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for steering
            value
            data_sending_event (multiprocessing.Event): Event for data transmission initiation
            connection_event (multiprocessing.Event): Event for initial tcp connection success
            stop_event (multiprocessing.Event): Event for stop signal
            emergency_stop_event (multiprocessing.Event): Event for emergency stop signal
            startup_barrier (multiprocessing.Barrier): Barrier that synchronizes startup of all processes
            img_roi (Tuple[int, int, int, int]): Shape of image's region of interest (x, y, w, h)
            max_detectable_lanes (int): Maximum number of lanes that can be detected at a time
            max_detectable_obj (int): Maximum number of objects that can be detected at a time
            ultrasonic_sensors (Dict): Configuration data on ultrasonic sensors
            izmq_port (int): Port for ImageZMQ service
            jpg_quality (int): Quality level of JPG compression [0, 100]
        """
        
        self.img_roi = img_roi
        self.izmq_port = izmq_port
        self.max_detectable_lanes = max_detectable_lanes
        self.max_detectable_obj = max_detectable_obj
        self.ultrasonic_sensors = ultrasonic_sensors
        self.jpg_quality = jpg_quality
        self.shared_remote_ip_address_data_ptr = shared_remote_ip_address_data_ptr
        
        # init events and barriers
        self.data_sending_event = data_sending_event
        self.connection_event = connection_event
        self.stop_event = stop_event
        self.emergency_stop_event = emergency_stop_event
        self.startup_barrier = startup_barrier
        
        # init process
        shared_memory_pointers = (shared_working_frequency_data_ptr, shared_image_data_cpy_ptr, 
                                  shared_lane_data_cpy_ptr, shared_object_data_cpy_ptr, 
                                  shared_object_certainty_data_cpy_ptr, shared_ultrasonic_data_cpy_ptr, 
                                  shared_control_speed_data_ptr, shared_control_steering_data_ptr)
        super().__init__(shared_memory_pointers, emergency_stop_event)
        
    def _init(self):
        """Initializes ImageZMQ Sender process specifically."""
        
        # wait until tcp connection is established
        self.connection_event.wait()

        # take remote ip address from shared memory
        shared_remote_ip_address_data = np.frombuffer(self.shared_remote_ip_address_data_ptr, dtype=np.uint8)
        ip_address_length = shared_remote_ip_address_data[0]  
        remote_ip_address = shared_remote_ip_address_data[1:ip_address_length+1].tobytes().decode()
        self.logger.debug("Remote IPv4 address: %s" % remote_ip_address)
        
        # initialize izmq socket
        self.logger.info("Initializing ImageZMQ socket...")
        izmq_uri = "tcp://%s:%d" % (remote_ip_address, self.izmq_port)
        self.image_sender = imagezmq.ImageSender(izmq_uri, True)
        us_msg_template = {name: -1. for name, config in self.ultrasonic_sensors.items() if int(config["ACTIVE"])}
        self.msg_data = {"TIME": "",
                         "WORK_FREQ": -1.,
                         "ENGINE": -1, 
                         "STEER": -1, 
                         "US": us_msg_template, 
                         "LANE_DETC": [],
                         "OBJ_DETC": []}
        self.logger.info("ImageZMQ socket has been initialized.")
        
        
    def _run(self, shared_working_frequency_data_ptr: Any, shared_image_data_cpy_ptr: Any, 
             shared_lane_data_cpy_ptr: Any, shared_object_data_cpy_ptr: Any, 
             shared_object_certainty_data_cpy_ptr: Any, shared_ultrasonic_data_cpy_ptr: Any, 
             shared_control_speed_data_ptr: Any, shared_control_steering_data_ptr: Any):
        """
        Cyclic worker method, collects data from different processes and sends to remote.

        Args:
            shared_working_frequency_data_ptr (multiprocessing.Value[ctypes.c_float]): Shared memory pointer for working
            frequency
            shared_image_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint8]): Shared memory pointer for 
            OpenCV image copy
            shared_lane_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer for 
            lane data copy
            shared_object_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_uint16]): Shared memory pointer 
            for object data copy
            shared_object_certainty_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared 
            memory pointer for object distances copy
            shared_ultrasonic_data_cpy_ptr (multiprocessing.sharedctypes.RawArray[ctypes.c_float]): Shared memory 
            pointer for ultrasonic data copy
            shared_control_speed_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for speed value
            shared_control_steering_data_ptr (multiprocessing.Value[ctypes.c_uint8]): Shared memory pointer for steering
            value
        """
        
        json_message = None
        jpg_buffer = None
        
        # create numpy arrays, representing views onto shared memory
        shared_img_data_cpy = np.frombuffer(shared_image_data_cpy_ptr, dtype=np.uint8).reshape(self.img_roi[3],
                                                                                               self.img_roi[2], 3)
        shared_lane_data_cpy = np.frombuffer(shared_lane_data_cpy_ptr, dtype=np.uint16).reshape(self.max_detectable_lanes, 4)
        shared_object_data_cpy = np.frombuffer(shared_object_data_cpy_ptr, 
                                               dtype=np.uint16).reshape((self.max_detectable_obj, 5))
        shared_object_certainty_data_cpy = np.frombuffer(shared_object_certainty_data_cpy_ptr, dtype=np.float32)
        shared_ultrasonic_data_cpy = np.frombuffer(shared_ultrasonic_data_cpy_ptr, dtype=np.float32)

        
        # wait until all processes are ready on startup
        self.logger.debug("Waiting for Startup...")
        self.startup_barrier.wait()
        
        while True:
            # wait until data copying has been finished
            self.logger.debug("Waiting for E_5...")
            self.data_sending_event.wait()
            self.logger.debug("E_5 set. Collecting data...")
            
            # wait, if stop signal was given by remote
            if not self.stop_event.is_set():
                self.logger.info("Stopped until remote gives signal to proceed.")
            self.stop_event.wait() 
                
            # structure data for transmission
            self.msg_data["TIME"] = datetime.now().strftime("%H:%M:%S.%f")[:-3]
            self.msg_data["WORK_FREQ"] = round(shared_working_frequency_data_ptr.value, 2)
            self.msg_data["ENGINE"] = int(shared_control_speed_data_ptr.value)
            self.msg_data["STEER"] = int(shared_control_steering_data_ptr.value)
            idx = 0
            for us_sensor_name, us_sensor_config in self.ultrasonic_sensors.items():
                if int(us_sensor_config["ACTIVE"]):
                    self.msg_data["US"][us_sensor_name] = round(float(shared_ultrasonic_data_cpy[idx]), 3)
                    idx += 1
            self.msg_data["LANE_DETC"].clear()
            for idx in range(self.max_detectable_lanes):
                x1, y1, x2, y2 = shared_lane_data_cpy[idx]
                if x1 == 0 and y1 == 0 and x2 == 0 and y2 == 0:
                    break
                lane_dict = {
                    "X1": int(x1),
                    "Y1": int(y1),
                    "X2": int(x2),
                    "Y2": int(y2)
                    }
                self.msg_data["LANE_DETC"].append(lane_dict)
            self.msg_data["OBJ_DETC"].clear()
            for idx in range(self.max_detectable_obj):
                if not any(shared_object_data_cpy[idx][1:]):
                    continue
                y0, x0, y1, x1 = shared_object_data_cpy[idx][1:]
                obj_dict = {"NAME": Object(shared_object_data_cpy[idx, 0]).name,
                            "CERT": round(float(shared_object_certainty_data_cpy[idx]) * 100, 2),
                            "BB_X0": int(x0),
                            "BB_Y0": int(y0),
                            "BB_X1" : int(x1),
                            "BB_Y1": int(y1)}
                self.msg_data["OBJ_DETC"].append(obj_dict)

            # encode structured data to json
            json_message = json.dumps(self.msg_data)

            _, jpg_buffer = cv2.imencode(".jpg", shared_img_data_cpy, [int(cv2.IMWRITE_JPEG_QUALITY), self.jpg_quality])
            self.logger.debug("Sending data...")
            self.image_sender.send_jpg(json_message, jpg_buffer)
            self.logger.debug("Data sent.")
            
            # reset waiting event
            self.data_sending_event.clear()
        
    def _on_termination(self):
        """Is invoked on process termination. Releases ImageZMQ socket resources."""
        
        # check if socket is set up
        if self.image_sender is not None:
            # close izmq socket and release resources
            self.logger.info("Closing ImageZMQ socket...")
            self.image_sender.close()
            self.logger.info("ImageZMQ socket has been closed.")
