import cv2
import numpy as np
import time
import os 

def init_cam():
    gstr_pipe = 'nvarguscamerasrc wbmode=2 saturation=1.2 exposuretimerange="30000000 30000000" gainrange="5 5"  ispdigitalgainrange="1 1" ! \
                     video/x-raw(memory:NVMM), width=%d, height=%d, format=(string)NV12, framerate=%s ! \
                     nvvidconv flip-method=2 ! \
                     video/x-raw, width=%d, height=%d, format=(string)BGRx ! \
                     videoconvert ! \
                     video/x-raw, format=(string)BGR ! \
                     appsink' % \
                    (1920, 1080, "30/1", 640, 360)
    camera = cv2.VideoCapture(gstr_pipe, cv2.CAP_GSTREAMER)
    return camera

def capture_calib_img(camera):
    count = 0
    print("ready to rumble")
    for i in range(1):
        for j in range(0):
            print("%d" % (3-j))
            time.sleep(1)

        capture_success, image = camera.read()
        
        while os.path.isfile("./final_calib/%04d.png" % count):
            count += 1
        cv2.imwrite("./final_calib/%04d.png" % count, image)
        print("\n")

def capture_images_with_different_interpolations(camera):
    mapx = np.loadtxt("../cam_calib/params/mapx.csv", delimiter=',', dtype=np.int16)
    mapx = np.reshape(mapx, (mapx.shape[0] // 2, mapx.shape[1], 2))
    mapy = np.loadtxt("../cam_calib/params/mapy.csv", delimiter=',', dtype=np.uint16)
    ROI = [14, 20, 607, 313]
    
    interpolation_flags = {"nearest": cv2.INTER_NEAREST, "linear": cv2.INTER_LINEAR, "cubic": cv2.INTER_CUBIC, "lanczos": cv2.INTER_LANCZOS4}
    
    capture_success, image = camera.read()
    

    for interpolation_name, interpolation_flag in interpolation_flags.items():
        times = []
        dst = None
        for rep in range(100):
            t1 = time.time()
            dst = cv2.remap(image, mapx, mapy, interpolation_flag)
            t2 = time.time()
            times.append(t2 - t1)
            x,y,w,h = ROI
            dst = dst[y:y+h, x:x+w]
        cv2.imwrite("../interpolation_comparison/%s.png" % interpolation_name, dst)
        avg_time = np.mean(times) * 1e3
        std_time = np.std(times, ddof=1) *1e3
        print("%s: %f ms ± %f ms" % (interpolation_name, avg_time, std_time))

if __name__ == "__main__":
    camera = init_cam()
    if not camera.isOpened():
        print("Cam init failed")
        exit()
    #capture_calib_img(camera)
    capture_images_with_different_interpolations(camera)
    camera.release()